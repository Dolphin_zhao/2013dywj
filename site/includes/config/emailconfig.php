<?php
 /**
 * Email Config for send email.
 * @package		includes.config
 * @author gary wang (wangbaogang123@hotmail.com)
*/
class EmailConfig{

	var $host      = '{$host}';
	var $from 	   = '{$from}';
	var $fromemail = '{$fromemail}';
	var $fromname  = '{$fromname}';
	var $passwd    = '{$password}';
	var $charset   = 'UTF-8';
	var $altbody   = 'text/html';

}
?>