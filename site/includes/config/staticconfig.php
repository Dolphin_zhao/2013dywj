<?php
 /**
 * Static Config
 * notice: search pattern by url index and pattern['source'],so if find error please check getIndex and cleanPath method also check HtmlHelper html_preg_replace method
 * @package		includes.config
 * @author gary wang (wangbaogang123@hotmail.com)
*/
class StaticConfig{

	protected $pattern	= array();
	
	public function productPath()
	{
		return Config::homedir().DS."product";
	}
	
	/**
	 * If diffrent url,please change this
	 */
	public function homeurl()
	{
		return Config::homeurl();
	}
	
	/**
	 * Initialize html content replace dynamic path
	 */
	public function __construct()
	{
		//$this->sourcePattern[] = "/news_detail\.php\?nid\=(\d+)/";
		//$this->replacePattern[] = "news_detail_\\1.html";
	}
	
	/**
	 * Add Pattern.
	 *
	 * @param	array $pattern 2 dimension array
	 * 
	 */
	public function addPattern($pattern)
	{
		if(is_array($pattern)){
			foreach($pattern as $item)
			{
				if(is_array($item)){
					if(array_key_exists("source",$item)&&array_key_exists("replace",$item)){
						$this->pattern[]  = $item;
					}
				}
			}
		}
	}
	
	/**
	 * get $url index in array
	 */
	public function getIndex($url)
	{
		$parts = parse_url($url);
		if(!array_key_exists("path",$parts)){
			return urlencode($url);
		}
		return urlencode($parts["path"]);
	}
	
	/**
	 * get indexed pattern
	 */
	public function getPattern()
	{
		//var_dump($this);
		//var_dump($this->pattern);
		$pattern = array();
		foreach($this->pattern as $item)
		{
			$key = $this->getIndex($this->cleanPath($item["source"]));
			$pattern[$key] = $item;
		}
		
		return $pattern;
	}
	
	/**
	 * clean reg path
	 */
	private function cleanPath($url){
		$index = str_replace("\\", "", $url);
		$index = substr($index,1,-1);
		return $index;
	}
}
?>
