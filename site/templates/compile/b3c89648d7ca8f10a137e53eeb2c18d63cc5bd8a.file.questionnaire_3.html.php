<?php /* Smarty version Smarty-3.0.4, created on 2013-01-04 16:59:55
         compiled from "/usr/local/www/Daning_new/2012dywj/site/templates/test99/index/questionnaire_3.html" */ ?>
<?php /*%%SmartyHeaderCode:125427196350e69a0b81f831-03292040%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b3c89648d7ca8f10a137e53eeb2c18d63cc5bd8a' => 
    array (
      0 => '/usr/local/www/Daning_new/2012dywj/site/templates/test99/index/questionnaire_3.html',
      1 => 1357289461,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '125427196350e69a0b81f831-03292040',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>大宁国际商业广场调研问卷</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.greyText {
    background-color:#DBE5E5;
};
</style>
<!--[if IE 6]>
<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
<script>DD_belatedPNG.fix('*'); </script>
<![endif]-->
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/checkFrom.js" type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8753756-28']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="main">
<div class="top"><a href="http://www.daningdaning.com/cn/" target="_blank" title="大宁国际"></a></div>
<div class="hd">
<div class="tt4"></div>
<div class="hd1"><img src="css/img/m1.gif" /><img src="css/img/m2_2.gif" /><img src="css/img/m3_1.gif" /><img src="css/img/m4.gif" /><img src="css/img/m5.gif" /></div>
</div>
<div class="center_1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="center_2">
  <tr>
    <td valign="top" class="center_3">
    <div class="center_5">
      <table width="855" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="word1"><span class="word5">C1</span> 请问您平均多久来大宁国际商业广场一次？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="118"><input type="radio" name="c_1" value="每月不到1次" /> 每月不到1次</td>
              <td width="106"><input type="radio" name="c_1" value="每月1-2次" /> 每月1-2次</td>
              <td width="106"><input type="radio" name="c_1" value="每月3-4次" /> 每月3-4次</td>
              <td width="492"><input type="radio" name="c_1" value="每月4次以上" /> 每月4次以上</td>
            </tr>
          </table></td>
          </tr>
          
        <tr>
          <td class="word1"><span class="word5">C2</span> 您来大宁国际商业广场主要是为了？ </td>
        </tr>          
          <tr>
          <td class="word4 pa1">
          <table width="600" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="200" height="52" class="line1">&nbsp;</td>
              <td width="98" align="center" class="line1">经常去
              <br /> <span class="word6">（每月2次以上）</span></td>
              <td width="125" align="center" class="line1">偶尔去<br />
                <span class="word6">（每月1次或更少）</span></td>
              <td width="129" align="center" class="line2">基本不去</td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　用餐</td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_1" value="用餐：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_1" value="用餐：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="c_2_1" value="用餐：基本不去" />
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　服装购买</td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_2" value="服装购买：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_2" value="服装购买：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="c_2_2" value="服装购买：基本不去" /></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　娱乐休闲（电影，KTV）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_3" value="娱乐休闲（电影，KTV）：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_3" value="娱乐休闲（电影，KTV）：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="c_2_3" value="娱乐休闲（电影，KTV）：基本不去" /></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　购买鞋帽等</td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_4" value="购买鞋帽等：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_4" value="购买鞋帽等：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="c_2_4" value="购买鞋帽等：基本不去" /></td>
            </tr> 
            
             <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　带孩子游玩</td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_5" value="带孩子游玩：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_5" value="带孩子游玩：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="c_2_5" value="带孩子游玩：基本不去" /></td>
            </tr>
            
             <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　大卖场（大润发）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_6" value="大卖场（大润发）：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_6" value="大卖场（大润发）：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="c_2_6" value="大卖场（大润发）：基本不去" /></td>
            </tr> 
            
             <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　咖啡西点冷饮</td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_7" value="咖啡西点冷饮：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_7" value="咖啡西点冷饮：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="c_2_7" value="咖啡西点冷饮：基本不去" /></td>
            </tr> 
            
             <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　其他</td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_8" value="其他：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="c_2_8" value="其他：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="c_2_8" value="其他：基本不去" /></td>
            </tr>                                                                   
          </table></td>
          </tr>          
          
          
        <tr>
          <td class="word1"><span class="word5">C3</span> 您来大宁国际消费的主要原因为？（可多选，最多三项）</td>
        </tr>
         <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="230" height="28"><input type="checkbox" name="c_3" class="c_3" value="购物环境好，氛围时尚" /> 购物环境好，氛围时尚</td>
              <td width="592" height="28"><input type="checkbox" name="c_3" class="c_3" value="商业业态较全" /> 商业业态较全</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="c_3" class="c_3" value="交通便利" /> 交通便利</td>
              <td height="28"><input type="checkbox" name="c_3" class="c_3" value="消费适中，品牌较为喜欢" /> 消费适中，品牌较为喜欢</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="c_3" class="c_3" value="有会员卡可以打折" /> 有会员卡可以打折</td>
              <td height="28"><input type="checkbox" name="c_3" class="c_3" value=" 离公司近，下班路过" /> 离公司近，下班路过</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="c_3" class="c_3" value="离家较近，比较方便"/> 离家较近，比较方便</td>
              <td height="28"><input type="checkbox" name="c_3" class="c_3" value="停车便利" /> 停车便利</td>
            </tr>
          </table></td>
        </tr>
          
        <tr>
          <td class="word1"><span class="word5">C4</span> 请问您一般通过什么样的交通工具来大宁国际商业广场？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="77" height="28"><input type="radio" name="c_4" value="自驾车" /> 自驾车</td>
              <td width="77" height="28"><input type="radio" name="c_4" value="出租车" /> 出租车</td>
              <td width="65" height="28"><input type="radio" name="c_4" value="地铁" /> 地铁 </td>
              <td width="77"><input type="radio" name="c_4" value="公交车" /> 公交车</td>
              <td width="77"><input type="radio" name="c_4" value="自行车" /> 自行车</td>
              <td><input type="radio" name="c_4" value="步行" /> 步行</td>
            </tr>

          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">C5</span> 请问您来大宁国际商业广场一般花费多少交通时间？</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="110"><input type="radio" name="c_5" value="10分钟以内" /> 10分钟以内</td>
              <td width="112"><input type="radio" name="c_5" value="10-20分钟" /> 10－20分钟</td>
              <td width="112"><input type="radio" name="c_5" value="20-30分钟" /> 20－30分钟</td>
              <td width="112"><input type="radio" name="c_5" value="30-60分钟" /> 30－60分钟</td>
              <td><input type="radio" name="c_5" value="60分钟以上" /> 60分钟以上</td>
              </tr>
          </table></td>
        </tr>
        
        
        <tr>
          <td class="word1"><span class="word5">C6</span> 请问您会为打折或特卖信息特意来大宁国际消费么？</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="90"><input type="radio" name="c_6" value="一般会" /> 一般会</td>
              <td width="160"><input type="radio" name="c_6" value="有时会，看品牌吧" /> 有时会，看品牌吧</td>
              <td width="200"><input type="radio" name="c_6" value="没所谓，遇见了就买点" /> 没所谓，遇见了就买点</td>
              <td><input type="radio" name="c_6" value="不关心" /> 不关心</td>
              </tr>
          </table></td>
        </tr>   
        
        
        <tr>
          <td class="word1"><span class="word5">C7</span> 请问打折与促销对您在哪方面的消费影响较大？</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="100"><input type="radio" name="c_7" value="餐饮" /> 餐饮</td>
              <td width="100"><input type="radio" name="c_7" value="服装" /> 服装</td>
              <td width="100"><input type="radio" name="c_7" value="大卖场" /> 大卖场</td>
              <td width="200"><input type="radio" name="c_7" value="生活服务（美容美发等）" /> 生活服务（美容美发等）</td>
              <td><input type="radio" name="c_7" value="家用电器" /> 家用电器</td>
              </tr>
          </table></td>
        </tr> 
        
        
        
        <tr>
          <td class="word1"><span class="word5">C8</span> 请问您每月在大宁国际的消费大概有多少？（估算）</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="100"><input type="radio" name="c_8" value="500元以内" /> 500元以内</td>
              <td width="120"><input type="radio" name="c_8" value="500－1000元" /> 500－1000元</td>
              <td width="120"><input type="radio" name="c_8" value="1000－2000元" /> 1000－2000元</td>
              <td width="120"><input type="radio" name="c_8" value="2000－3000元" /> 2000－3000元</td>
              <td><input type="radio" name="c_8" value=" 3000元以上 " />  3000元以上 </td>
              </tr>
          </table></td>
        </tr> 

        
        <tr>
          <td class="word1"><span class="word5">C9</span> 请问您一般从哪里得知大宁国际的折扣及活动信息？（可多选，最多三项）</td>
        </tr>
         <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="230" height="28"><input type="checkbox" name="c_9" class="c_9" value="电视电台" /> 电视电台</td>
              <td width="592" height="28"><input type="checkbox" name="c_9" class="c_9" value="商户宣传" /> 商户宣传</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="c_9" class="c_9" value="报刊杂志" /> 报刊杂志</td>
              <td height="28"><input type="checkbox" name="c_9" class="c_9" value="询问处及宣传册" /> 询问处及宣传册</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="c_9" class="c_9" value="户外及广场内广告" /> 户外及广场内广告</td>
              <td height="28"><input type="checkbox" name="c_9" class="c_9" value=" 微博及网络" /> 微博及网络</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="c_9" class="c_9" value="会员短信与邮件"/> 会员短信与邮件</td>
              <td height="28"><input type="checkbox" name="c_9" class="c_9" value="活动现场 " /> 活动现场 </td>
            </tr>
          </table></td>
        </tr>  
        


   <tr>
          <td class="word1"><span class="word5">C10</span> 请问您知道或参加过下列任何大宁国际的推广（广场）活动么？</td>
        </tr> 
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="30">&nbsp;</td>
              <td width="231" height="30" valign="top"><input type="radio" value="有" name="c_10" class="myhave c_join_what_select" id="c10_y" /> 有，请选择</td>
              <td width="591" height="30" valign="top"><input type="radio" value="没有" name="c_10" class="mynone c_join_what_select" id="c10_n" /> 没有，跳过</td>
            </tr>
            
            <tr>
               <td height="28">&nbsp;</td>
              <td width="591" height="30" colspan="2">请选择您最喜欢的推广活动：（可多选，最多三项）</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28" width="35%"><input type="checkbox" name="c_10_1" class="c_10_1" value="2011圣诞新年 Bwing艺术装置展" data="1" /> 1. 2011圣诞新年 Bwing艺术装置展</td>
              <td height="28"><input type="checkbox" name="c_10_1" class="c_10_1" value="圣诞新年 倒计时音乐派对（牛奶咖啡）" data="2" /> 2. 圣诞新年 倒计时音乐派对（牛奶咖啡）</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28" width="35%"><input type="checkbox" name="c_10_1" class="c_10_1" value="宁的中国年 李守白剪纸艺术装置展" data="3" /> 3. 宁的中国年 李守白剪纸艺术装置展</td>
              <td height="28"><input type="checkbox" name="c_10_1" class="c_10_1" value="宁的中国年春节系列活动（大宁夜、元宵节）" data="4" /> 4. 宁的中国年春节系列活动（大宁夜、元宵节）</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28" width="35%"><input type="checkbox" name="c_10_1" class="c_10_1" value="春日芬芳秀 四维气味装置艺术展" data="5" /> 5. 春日芬芳秀 四维气味装置艺术展</td>
              <td height="28"><input type="checkbox" name="c_10_1" class="c_10_1" value="大宁国际儿童节 中华娃娃喜乐会" data="6" /> 6. 大宁国际儿童节 中华娃娃喜乐会</td>
            </tr>

            <tr>
              <td height="28">&nbsp;</td>
              <td height="28" width="35%"><input type="checkbox" name="c_10_1" class="c_10_1" value="大人少年宫 梦想飞行那些年（星尚合作）" data="7" /> 7. 大人少年宫 梦想飞行那些年（星尚合作）</td>
              <td height="28"><input type="checkbox" name="c_10_1" class="c_10_1" value="英国女王登基60周年快闪 （英国旅发局合作）" data="8" /> 8. 英国女王登基60周年快闪 （英国旅发局合作）</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28" width="35%"><input type="checkbox" name="c_10_1" class="c_10_1" value="大宁运动会 伦敦奥运主题活动" data="9" /> 9. 大宁运动会 伦敦奥运主题活动</td>
              <td height="28"><input type="checkbox" name="c_10_1" class="c_10_1" value="旅游节花车巡游" data="10" /> 10. 旅游节花车巡游</td>
            </tr>  
            
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28" width="35%"><input type="checkbox" name="c_10_1" class="c_10_1" value="&quot;跨越音乐 联动城市&quot;" data="11" /> 11. 香港、台北、北京、上海四城联动展</td>
              <td height="28"><input type="checkbox" name="c_10_1" class="c_10_1" value="香港、台北、北京、上海四城联动展" data="12" /> 12. 大宁音乐季 苏打绿、卢巧音、彭坦、GAIA乐团</td>
            </tr>  
            
          
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28" colspan="2"> 请为以上勾选项做排序 <input type="text" name="c_10_2" id="c_10_2"  size="5"/>  <input type="text" name="c_10_3" id="c_10_3"  size="5"/>  <input type="text" name="c_10_4" id="c_10_4"  size="5"/></td>
            </tr>                                                   
          </table></td>
        </tr>

        
        
        <tr>
          <td class="word1"><span class="word5">C11</span> 请问您经常使用大宁卡么？ </td>
        </tr>
         <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">

            <tr>
              <td width="33">&nbsp;</td>
              <td height="28" width="250"><input type="radio" name="c_11" value="基本不用，觉得没什么用处" /> 基本不用，觉得没什么用处</td>
              <td height="28"><input type="radio" name="c_11" value="用处不大，积分比较麻烦，偶尔用用" /> 用处不大，积分比较麻烦，偶尔用用</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="radio" name="c_11" value="可以打折的商家就使用" /> 可以打折的商家就使用</td>
              <td height="28"><input type="radio" name="c_11" value="基本每次都用，并会积分" /> 基本每次都用，并会积分</td>
            </tr>            

          </table></td>
        </tr> 
        
        
        <tr>
          <td class="word1"><span class="word5">C12</span> 请问哪项会员卡服务您最感兴趣？（可多选，最多两项）</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="100"><input type="checkbox" name="c_12" class="c_12" value="商家折扣" /> 商家折扣</td>
              <td width="100"><input type="checkbox" name="c_12" class="c_12" value="积分换礼" /> 积分换礼</td>
              <td width="100"><input type="checkbox" name="c_12" class="c_12" value="免费停车" /> 免费停车</td>
              <td width="150"><input type="checkbox" name="c_12" class="c_12" value="vip会员活动" /> vip会员活动</td>
              <td><input type="checkbox" name="c_12" class="c_12" value="特卖积分抵扣现金" /> 特卖积分抵扣现金</td>
              </tr>
          </table></td>
        </tr>
        
        <tr>
          <td class="word1"><span class="word5">C13</span> 请问作为大宁国际会员客户，您对大宁卡最主要的意见是？（可多选，最多两项）</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="80%" border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td width="33" height="28">&nbsp;</td>
              <td><input type="checkbox" name="c_13" class="c_13" value="会员折扣优惠还不错，是我来大宁消费的主要理由之一" /> 会员折扣优惠还不错，是我来大宁消费的主要理由之一</td>
            </tr>
            <tr>
               <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="c_13" class="c_13" value="会员服务与活动有一定吸引力" /> 会员服务与活动有一定吸引力</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="c_13" class="c_13" value="会员服务与优惠较少，积分吸引力不大" /> 会员服务与优惠较少，积分吸引力不大</td>
            </tr> 
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="c_13" class="c_13" value="会员权利及优惠等不明晰，积分用处不大，刺激不了我的消费" /> 会员权利及优惠等不明晰，积分用处不大，刺激不了我的消费</td>
            </tr> 
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="c_13" class="c_13" value="看不出有什么用，有没有无所谓" /> 看不出有什么用，有没有无所谓</td>
            </tr>                                                 
          </table></td>
        </tr>
        
        
        <tr>
          <td class="word1"><span class="word5">C14</span> 请问您最近一年来，来大宁的次数有什么变化？ </td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="80%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="150"><input type="radio" name="c_14" value="比去年少了点" /> 比去年少了点</td>
              <td width="150"><input type="radio" name="c_14" value="和去年差不多吧" /> 和去年差不多吧</td>
              <td><input type="radio" name="c_14" value="比去年来得频繁了" /> 比去年来得频繁了</td>
              </tr>
          </table></td>
        </tr> 
   
        <tr>
          <td ><a class="a1" id="nextFour" href="javascript:void(0)">下一步</a></td>
        </tr>
      </table>
    </div>
    </td>
  </tr>
  <tr>
    <td height="38" class="center_4">&nbsp;</td>
  </tr>
</table>
</div>
</div>
<script language="JavaScript">
$(document).ready(function(){
    $("#c_10_2, #c_10_3, #c_10_4").hide();    
    
    $("#c10_y").click(function(){
        $('input[name=c_10_1]').each(function(){
            $(this).attr( "disabled", "");
        });
        $("#c_10_2, #c_10_3, #c_10_4").attr("disabled", "");      
    })
    
    $("#c10_n").click(function(){
        $('input[name=c_10_1]').each(function(){
            $(this).attr( "disabled", "disabled").attr('checked', false);
            //$(this).attr('checked', false);            
        });
        $("#c_10_2, #c_10_3, #c_10_4").attr("disabled", "disabled").val('').hide();                    
    })  
        
    $('.c_3, .c_9').click(function(){
        if (!getCheckBoxLitmit('c_3', 3) || !getCheckBoxLitmit('c_9', 3)) 
        {
            alert('最多只能选三项');
            return false;
        };    
    });
    
    $('.c_12, .c_13').click(function(){
        if (!getCheckBoxLitmit('c_12', 2) || !getCheckBoxLitmit('c_13', 2)) 
        {
            alert('最多只能选两项');
            return false;
        };    
    });
    
    $('.c_10_1').click(function(){
        if (!getCheckBoxLitmit('c_10_1', 3)) 
        {
            alert('最多只能选三项');
            return false;
        };
        
        var selectedCnt = getCheckBoxLitmit1('c_10_1');
        
        if (selectedCnt == 0)
        {
            $("#c_10_2, #c_10_3, #c_10_4").hide().val('');           
        }
        else if (selectedCnt == 1)
        {
            if(!$("#c_10_2").is(':visible'))
            {
                $("#c_10_2").show();                
            }
            
            $("#c_10_3, #c_10_4").hide().val('');  
        }
        else if (selectedCnt == 2)
        {
            if(!$("#c_10_2").is(':visible'))
            {
                $("#c_10_2").show();                
            }
            if(!$("#c_10_3").is(':visible'))
            {

                $("#c_10_3").show();                
            }
            
            $("#c_10_4").hide().val('');              
        } 
        else if (selectedCnt == 3)
        {
            $("#c_10_2, #c_10_3, #c_10_4").show();  
        }           
    });    
    
    $("#nextFour").click(function()
    {
    var c_1 = $('input:[name=c_1]:checked').val();
    var c_2_1 = $('input:[name=c_2_1]:checked').val();
    var c_2_2 = $('input:[name=c_2_2]:checked').val();
    var c_2_3 = $('input:[name=c_2_3]:checked').val();
    var c_2_4 = $('input:[name=c_2_4]:checked').val();
    var c_2_5 = $('input:[name=c_2_5]:checked').val();
    var c_2_6 = $('input:[name=c_2_6]:checked').val();
    var c_2_7 = $('input:[name=c_2_7]:checked').val();
    var c_2_8 = $('input:[name=c_2_8]:checked').val();
        
    var c_3 = getCheckBox('c_3');
    var c_4 = $('input:[name=c_4]:checked').val();
    var c_5 = $('input:[name=c_5]:checked').val();
    var c_6 = $('input:[name=c_6]:checked').val();
    var c_7 = $('input:[name=c_7]:checked').val();
    var c_8 = $('input:[name=c_8]:checked').val();
    var c_9  = getCheckBox('c_9');
    var c_10 = $('input:[name=c_10]:checked').val();
    var c_11 = $('input:[name=c_11]:checked').val();
    var c_12 = getCheckBox('c_12');
    var c_13 =  getCheckBox('c_13');
    var c_14 = $('input:[name=c_14]:checked').val(); 
    
    if(c_1 == undefined || c_1 == ''){
            alert('请回答C1');
            return false;
    }
    
    if(c_2_1 == undefined || c_2_1 == ''){
            alert('请回答C2');
            return false;
    }
    
    if(c_2_2 == undefined || c_2_2 == ''){
            alert('请回答C2');
            return false;
    } 
    
    if(c_2_3 == undefined || c_2_3 == ''){
            alert('请回答C2');
            return false;
    } 
    
    if(c_2_4 == undefined || c_2_4 == ''){
            alert('请回答C2');
            return false;
    } 
    
    if(c_2_5 == undefined || c_2_5 == ''){
            alert('请回答C2');
            return false;
    } 
    
    if(c_2_6 == undefined || c_2_6 == ''){
            alert('请回答C2');
            return false;
    } 
    
    if(c_2_7 == undefined || c_2_7 == ''){
            alert('请回答C2');
            return false;
    }
    
    if(c_2_8 == undefined || c_2_8 == ''){
            alert('请回答C2');
            return false;
    } 
    
                                
    
    if(c_3 == undefined || c_3 == ''){
            alert('请回答C3');
            return false;
    }
    
    if(c_4 == undefined || c_4 == ''){
            alert('请回答C4');
            return false;
    }  

    if(c_5 == undefined || c_5 == ''){
            alert('请回答C5');
            return false;
    } 
    
    if(c_6 == undefined || c_6 == ''){
            alert('请回答C6');
            return false;
    }
    
    if(c_7 == undefined || c_7 == ''){
            alert('请回答C7');
            return false;
    }  
    
    if(c_8 == undefined || c_8 == ''){
            alert('请回答C8');
            return false;
    }                    
                                         
    if(c_9 == undefined || c_9 == ''){
            alert('请回答C9');
            return false;
    }
    
    if(c_10 == undefined || c_10 == ''){
            alert('请回答C10');
            return false;
    }
    
    if (c_10 == '有')
    {
        c_10 =  getCheckBox('c_10_1');
            
        if(c_10 == undefined || c_10 == ''){
            alert('请回答C10');
            return false;
        }        
            //

        var c_10_2 = $('#c_10_2').val();
        var c_10_3 = $('#c_10_3').val();
        var c_10_4 = $('#c_10_4').val();
        
        if (isNaN(c_10_2) || isNaN(c_10_3) || isNaN(c_10_4))
        {
            alert('C10 活动序号必须为数字');
            $('input:[name=c_10_2]').focus();
            return false;
        }
        
       var temp = new Array();
        
        $(".c_10_1").each(function(){
            $this = $(this);
            if ($this.attr('checked') == true)
            {
                temp.push($this.attr('data'));    
            }
        });
        

        
        var t = temp.join(",");
        if (c_10_2 != '')
        {
            if (t.indexOf(c_10_2) == -1)
            {
                alert('C10 活动序号必须在在您选择的数字中间');
                $('#c_10_2').focus(); 
                return false;                                  
            }
            
            if (c_10_2 < 1 || c_10_2 > 15)
            {
                alert('C10 活动序号必须在1~15之间');
                $('#c_10_2').focus(); 
                return false;                 
            }
        }
        
        if (c_10_3 != '')
        {
            if (t.indexOf(c_10_3) == -1)
            {
                alert('C10 活动序号必须在在您选择的数字中间');
                $('#c_10_3').focus(); 
                return false;                                  
            }
            
            if (c_10_3 < 1 || c_10_3 > 15)
            {
                alert('C10 活动序号必须在1~15之间');
                $('#c_10_3').focus(); 
                return false;                 
            }            
        } 
        
        if (c_10_4 != '')
        {
            if (t.indexOf(c_10_4) == -1)
            {
                alert('C10 活动序号必须在在您选择的数字中间');
                $('#c_10_4').focus(); 
                return false;                                  
            }
            
            if (c_10_4 < 1 || c_10_4 > 12)
            {
                alert('C10 活动序号必须在1~12之间');
                $('#c_10_4').focus(); 
                return false;                 
            }             
        }
        
        if (c_10_2 != '' && c_10_3 != '')
        {
            if (c_10_2 == c_10_3)
            {
                alert('C10 活动序号不能重复');
                $('#c_10_3').focus();
                return false;
            }
        }
        
        if (c_10_2 != '' && c_10_4 != '')
        {
            if (c_10_2 == c_10_4)
            {
                alert('C10 活动序号不能重复');
                $('#c_10_4').focus();
                return false;
            }
        }  
        
        if (c_10_3 != '' && c_10_4 != '')
        {
            if (c_10_3 == c_10_4)
            {
                alert('C10 活动序号不能重复');
                $('#c_10_4').focus();
                return false;
            }
        }
        
        if (temp.length == 1)
        {
            if (c_10_2 == '')
            {
                alert('C10 活动序号必须填写');
                $('#c_10_2').focus();
                return false;
            }
        }
        
        if (temp.length == 2)
        {
            if (c_10_2 == '')
            {
                alert('C10 活动序号必须填写');
                $('#c_10_2').focus();
                return false;
            }
            
            if (c_10_3 == '')
            {
                alert('C10 活动序号必须填写');
                $('#c_10_3').focus();
                return false;
            }
        }
        
        if (temp.length == 3)
        {
            if (c_10_2 == '')
            {
                alert('C10 活动序号必须填写');
                $('#c_10_2').focus();
                return false;
            }
            
            if (c_10_3 == '')
            {
                alert('C10 活动序号必须填写');
                $('#c_10_3').focus();
                return false;
            }
            
            if (c_10_4 == '')
            {
                alert('C10 活动序号必须填写');
                $('#c_10_4').focus();
                return false;
            }                         
        }                                                  
                
         
        if(c_10_2 != '' || c_10_3 != '' || c_10_4 != ''){
            c_10 += ", 活动序号：" + c_10_2 +','+ c_10_3 +","+ c_10_4;
        }       
    } 
    
    if(c_11 == undefined || c_11 == ''){
            alert('请回答C11');
            return false;
    }
    
    if(c_12 == undefined || c_12 == ''){
            alert('请回答C12');
            return false;
    }   
    
    if(c_13 == undefined || c_13 == ''){
            alert('请回答C13');
            return false;
    }  
    
    if(c_14 == undefined || c_14 == ''){
            alert('请回答C14');
            return false;
    }

        $.post("stepFour.php?t="+Math.random(),
            {
                c_1:c_1,
                c_2_1:c_2_1,
                c_2_2:c_2_2,
                c_2_3:c_2_3,
                c_2_4:c_2_4,
                c_2_5:c_2_5,
                c_2_6:c_2_6,
                c_2_7:c_2_7,
                c_2_8:c_2_8,
                c_3:c_3,
                c_4:c_4,
                c_5:c_5,
                c_6:c_6,
                c_7:c_7,
                c_8:c_8,
                c_9:c_9,
                c_10:c_10,
                c_11:c_11,
                c_12:c_12,
                c_13:c_13,
                c_14:c_14                
            },
            function(data){
                var item = eval("("+data+")");
                    if (item.result == "INPUT_INVALID") {
                        alert("表单填写不完整！");
                    } else if (item.result == "NONE") {
                        alert("会员卡号填写错误！");
                        window.location='index.php';
                        return false;
                    } else if (item.result == "EXIST") {
                        alert("您已参加过本次调研了！");
                        window.location='index.php';
                        return false;
                    } else if (item.result == "LIMIT") {
                        alert("感谢您的积极参与，本次活动已结束，如您有任何建议或意见，欢迎发送邮件至research@chongbang.com ");
                        window.location='index.php';
                        return false;
                    } else if (item.result == "OK") {
                        window.location='fivelist.php';
                        return false;
                    } else {
                        window.location='fourlist.php';
                        return false;
                    }
            });    
    });              
});
</script>
</body>
</html>
