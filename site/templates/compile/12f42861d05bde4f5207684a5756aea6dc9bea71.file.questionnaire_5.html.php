<?php /* Smarty version Smarty-3.0.4, created on 2013-01-04 17:09:17
         compiled from "/usr/local/www/Daning_new/2012dywj/site/templates/test99/index/questionnaire_5.html" */ ?>
<?php /*%%SmartyHeaderCode:131598288050e69c3d496c80-81762890%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '12f42861d05bde4f5207684a5756aea6dc9bea71' => 
    array (
      0 => '/usr/local/www/Daning_new/2012dywj/site/templates/test99/index/questionnaire_5.html',
      1 => 1357289460,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '131598288050e69c3d496c80-81762890',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>大宁国际商业广场调研问卷</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="colorbox/colorbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="colorbox/jquery.colorbox-min.js"></script>
<!--[if IE 6]>
<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
<script>DD_belatedPNG.fix('*'); </script>
<![endif]-->
<style type="text/css">
#markDiv { position:fixed; _position:absolute; z-index:9998; left:0; top:0; width:100%; height:100%; _height:1400px; background:#000; opacity:0.7; filter:alpha(opacity=80); -ms-filter:"alpha(opacity=80)"; display:none; }
#dialog_box { position:absolute; z-index:9999; left:50%; top:700px; width:800px; margin:0 0 0 -400px; text-align:center;  text-align:center; display:none;}
</style>
<script type="text/javascript">
$("#markDiv,#dialog_box").hide();
function tvc_show() {
   $("#markDiv,#dialog_box").toggle();
}
</script>
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/checkFrom.js" type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8753756-28']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
<div id="main">
<div class="top"><a href="http://www.daningdaning.com/cn/" target="_blank" title="大宁国际"></a></div>
<div class="hd">
<div class="tt6"></div>
<div class="hd1"><img src="css/img/m1.gif" /><img src="css/img/m2.gif" /><img src="css/img/m3.gif" /><img src="css/img/m4_2.gif" /><img src="css/img/m5_1.gif" /></div>
</div>
<div class="center_1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="center_2">
  <tr>
    <td valign="top" class="center_3">
    <div class="center_5">
      <table width="855" border="0" cellspacing="0" cellpadding="0">

        <tr>
          <td class="word1">
          <span class="word5">E1</span> 请选择你对大宁最迫切的建议？（可多选，最多两项）</td>
        </tr>
         <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="230" height="28"><input type="checkbox" name="e_1" class="e_1" value="增加餐饮种类" /> 增加餐饮种类</td>
              <td width="592" height="28"><input type="checkbox" name="e_1" class="e_1" value="增加服装种类" /> 增加服装种类</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="e_1" class="e_1" value="增加儿童消费" /> 增加儿童消费</td>
              <td height="28"><input type="checkbox" name="e_1" class="e_1" value="增加休闲娱乐" /> 增加休闲娱乐</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="e_1" class="e_1" value="改善服务水平" /> 改善服务水平</td>
              <td height="28"><input type="checkbox" name="e_1" class="e_1" value=" 具体还有哪些其他建议？" /> 具体还有哪些其他建议？<input type="text" size="30" name="e_1_1" maxlength="50" /></td>
            </tr>
          </table></td>
        </tr>
        
        <tr>
          <td class="word1"><span class="word5">E2</span> 请您在以下关键词中，挑出三个您对大宁国际最直接的印象：</td>
        </tr>
         <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="230" height="28"><input type="checkbox" name="e_2" class="e_2" value="时尚现代" /> 时尚现代</td>
              <td width="592" height="28"><input type="checkbox" name="e_2" class="e_2" value="设施偏旧" /> 设施偏旧</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value="品牌不错 " /> 品牌不错 </td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value="品牌一般" /> 品牌一般</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value="环境氛围好" /> 环境氛围好</td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value=" 环境一般" /> 环境一般</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value="比较独特"/> 比较独特</td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value="没什么特色" /> 没什么特色</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value="活动挺多"/> 活动挺多</td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value="没啥可买的" /> 没啥可买的</td>
            </tr>               
            
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value="挺好玩的"/> 挺好玩的</td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value="没新鲜感" /> 没新鲜感</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="e_2" class="e_2" value="离家近，没其他选择"/> 离家近，没其他选择</td>
           
            </tr>            
                     
            
          </table></td>
        </tr>  
        
        <tr>
          <td class="word1"><span class="word5">E3</span> 周边哪些商业设施您也会经常去？ </td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="120"><input type="checkbox" name="e_3" class="e_3" value="宝山万达广场" /> 宝山万达广场</td>
              <td width="105"><input type="checkbox" name="e_3" class="e_3" value="虹口龙之梦" /> 虹口龙之梦</td>
              <td width="105"><input type="checkbox" name="e_3" class="e_3" value="闸北大悦城" /> 闸北大悦城</td>
              <td><input type="checkbox" name="e_3" class="e_3" value="其他" /> 其他 <input type="text" size="30" name="e_3_1" maxlength="50" /></td>
             
              </tr>
          </table></td>
        </tr>              
               
            <tr>
              <td height="32" colspan="6" class="word1 pa2">您对大宁国际商业广场还有什么建议：</td>
              </tr>
            <tr>
              <td height="140" colspan="6" valign="top"><textarea name="e_4" id="e_4" cols="45" rows="5" class="textarea1"></textarea></td>
            </tr>

          </table>
            <table width="821" border="0" cellspacing="0" cellpadding="0">
            
              <tr>
                <td colspan="2"><div class="queren">&nbsp;</div></td>
                </tr>
              <tr>
                <td height="100"><a class="a2" href="javascript:void(0);" id="nextSix">提交</a></td>
                </tr>
            </table>
    </div>
    </td>
  </tr>
  <tr>
    <td height="38" class="center_4">&nbsp;</td>
  </tr>
</table>
</div>
</div>

<!--提交完毕-->
<div id="markDiv"></div>
<div id="dialog_box" style="display: none;">
<div class="tab_1">
<div class="c1">您已提交完毕！</div>
<div class="c2">感谢您的配合，我们的工作人员将与您取得联系</div>
<div class="pm" align="center"><a href="#" onclick="javascript:window.location='index.php'" class="a3">关闭</a></div>
</div>
</div>
<script language="JavaScript">
$(document).ready(function(){
    $('input:[name=e_2]').click(function(){
        if (!getCheckBoxLitmit('e_2', 3)) 
        {
            alert('最多只能选三项');
            return false;
        };    
    });
    
    $('.e_1').click(function(){
        if (!getCheckBoxLitmit('e_1', 2)) 
        {
            alert('最多只能选两项');
            return false;
        };    
    });         
    
    
	$("#nextSix").click(function(){
        var e_1     = getCheckBox('e_1');
        var e_1_1   = $('input:[name=e_1_1]').val();
        var e_2     = getCheckBox('e_2');
        var e_3     = getCheckBox('e_3');
        var e_3_1   = $('input:[name=e_3_1]').val();
        var e_4     = $('textarea:[name=e_4]').val();
		
        if (e_1 == undefined || e_1 == '') {
			alert('请回答E1');
            return false;
		}
        
        if (e_1.indexOf('具体还有哪些其他建议') != -1)
        {
            if (e_1_1 == undefined || e_1_1 == '') 
            {
                alert('请回答E1');
                $('input:[name=e_1_1]').focus();
                return false;
            }
        }
        e_1 += "："+e_1_1;           
        
        if (e_2 == undefined || e_2 == '') {
            alert('请回答E2');
            return false;
        }
        
        if (e_3 == undefined || e_3 == '') {
            alert('请回答E3');
            return false;
        } 
        
        if (e_3.indexOf('其他') != -1)
        {
            if (e_3_1 == undefined || e_3_1 == '') 
            {
                alert('请回答E3');
                $('input:[name=e_3_1]').focus();
                return false;
            }
        } 
        
        e_3 += "："+e_3_1;       
                                   

		
		$.post("stepSix.php?t="+Math.random(),
            {
                e_1:e_1,
                e_2:e_2,
                e_3:e_3,
                e_4:e_4
            },
            function(data){
                var item = eval("("+data+")");
                    if (item.result == "INPUT_INVALID") {
                        alert("表单填写不完整！");
                    } else if (item.result == "NONE") {
						alert("会员卡号填写错误！");
						window.location='index.php';
                        return false;
                    } else if (item.result == "EXIST") {
						alert("您已参加过本次调研了！");
						window.location = 'index.php';
                        return false;
                    } else if (item.result == "LIMIT") {
						alert("感谢您的积极参与，本次活动已结束，如您有任何建议或意见，欢迎发送邮件至research@chongbang.com ");
                        window.location='index.php';
                        return false;
                    } else if (item.result == "FAIL") {
						alert("记录失败，请重新填写！");
						window.location = 'index.php';
                        return false;
                    } else if (item.result == "OK") {
						$("#markDiv").show();
                        $("#dialog_box").show();
                    }
            }
        );
	});
});
</script>
</body>
</html>
