<?php /* Smarty version Smarty-3.0.4, created on 2013-01-04 16:57:46
         compiled from "/usr/local/www/Daning_new/2012dywj/site/templates/test99/index/questionnaire_2.html" */ ?>
<?php /*%%SmartyHeaderCode:104224498250e6998aa53431-26999275%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7a80b354fb77af57200ae4a9eb8545019bc5ac45' => 
    array (
      0 => '/usr/local/www/Daning_new/2012dywj/site/templates/test99/index/questionnaire_2.html',
      1 => 1357289461,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '104224498250e6998aa53431-26999275',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>大宁国际商业广场调研问卷</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
<script>DD_belatedPNG.fix('*'); </script>
<![endif]-->
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/checkFrom.js" type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8753756-28']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="main">
<div class="top"><a href="http://www.daningdaning.com/cn/" target="_blank" title="大宁国际"></a></div>
<div class="hd">
<div class="tt3"></div>
<div class="hd1"><img src="css/img/m1_2.gif" /><img src="css/img/m2_1.gif" /><img src="css/img/m3.gif" /><img src="css/img/m4.gif" /><img src="css/img/m5.gif" /></div>
</div>
<div class="center_1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="center_2">
  <tr>
    <td valign="top" class="center_3">
    <div class="center_5">
      <table width="855" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="word1"><span class="word5">B1</span> 请问您的家庭每月用于日常消费（服装，饮食，娱乐）占总收入的比例大概在？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="122"><input type="radio" name="b1" value="20％以下" /> 20％以下</td>
              <td width="118"><input type="radio" name="b1" value="20％－40％" /> 20％－40％</td>
              <td width="118"><input type="radio" name="b1" value="40％－60％" /> 40％－60％</td>
              <td><input type="radio" name="b1" value="60％以上" /> 60％以上</td>
              </tr>
          </table></td>
          </tr>
        <tr>
          <td class="word1"><span class="word5">B2</span> 请问您一般外出购物消费（包括购物，餐饮，电影，休闲等）的频率？ </td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
         <td width="33">&nbsp;</td>
              <td width="122"><input type="radio" name="b2" value="每周不到1次" /> 每周不到1次</td>
              <td width="118"><input type="radio" name="b2" value="每周1－2次" /> 每周1－2次</td>
              <td width="118"><input type="radio" name="b2" value="每周3－4次" /> 每周3－4次</td>
              <td><input type="radio" name="b2" value="每周5次及以上" /> 每周5次及以上</td>
              </tr>

          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">B3</span> 请问您购物与娱乐时一般和谁在一起？</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="70"><input type="radio" name="b3" value="自己" /> 自己</td>
              <td width="70"><input type="radio" name="b3" value="恋人" /> 恋人</td>
              <td width="70"><input type="radio" name="b3" value="朋友" /> 朋友</td>
              <td width="70"><input type="radio" name="b3" value="同事" /> 同事</td>
              <td><input type="radio" name="b3" value="家人和孩子" /> 家人和孩子</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">B4</span> 请问您每月在日常休闲购物方面的消费（包括外出就餐、娱乐、日用品与服装购买等）大概在多少？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="111" height="28"><input type="radio" name="b4" value="1000元以下" /> 1000元以下</td>
              <td width="124" height="28"><input type="radio" name="b4" value="1000-2000元" /> 1000-2000元</td>
              <td width="124" height="28"><input type="radio" name="b4" value="2000-3000元" /> 2000-3000元</td>
              <td width="124" height="28"><input type="radio" name="b4" value="3000-5000元" /> 3000-5000元</td>
              <td height="28"><input type="radio" name="b4" value="5000元以上" /> 5000元以上</td>
              </tr>

          </table></td>
        </tr>
        
        <tr>
          <td class="word1"><span class="word5">B5</span> 请问您经常网购么？</td>
        </tr> 
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="30">&nbsp;</td>
              <td width="231" height="30" valign="top"><input type="radio" value="是" name="b5" class="myhave c_join_what_select" id="b5_y" /> 是</td>
              <td width="591" height="30" valign="top"><input type="radio" value="不是" name="b5" class="mynone c_join_what_select" id="b5_n" /> 不是（跳过本题）</td>
            </tr>
            
            <tr>
               <td height="28">&nbsp;</td>
              <td width="591" height="30" colspan="2">如果是，经常购买的是以下哪一类产品？（可多选）</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="b5_1" class="c_join_what_class haveaction" value="服装" /> 服装</td>
              <td height="28"><input type="checkbox" name="b5_1" class="c_join_what_class haveaction" value="电子数码" /> 电子数码</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="b5_1" class="c_join_what_class haveaction" value="鞋子" /> 鞋子</td>
              <td height="28"><input type="checkbox" name="b5_1" class="c_join_what_class haveaction" value="生活百货" /> 生活百货</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="checkbox" name="b5_1" class="c_join_what_class haveaction" value="包和配饰类 " /> 包和配饰类 </td>
              <td height="28"><input type="checkbox" name="b5_1" class="c_join_what_class haveaction" value="饮料酒水食品类" /> 饮料酒水食品类</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28" nowrap><input type="checkbox" name="b5_1" class="c_join_what_class haveaction" value="其他" /> 其他  <input type="text" name="b5_2" id="b5_2"  size="30" maxlength="50" /></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">B6</span> 请问您经常使用APP类智能手机应用程序么？</td>
        </tr>
        <tr>
          <td  class="word4 pa">
          <table width="50%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td height="28" width="33">&nbsp;</td>
              <td width="70"><input type="radio" name="b6" value="是" /> 是</td>
              <td width="80"><input type="radio" name="b6" value="偶尔" /> 偶尔</td>
              <td><input type="radio" name="b6" value="一般不用" /> 一般不用</td>
              </tr>
          </table></td>
        </tr>
        
        <tr>
          <td class="word1"><span class="word5">B7</span> 请问您会经常从网上了解购物信息，或下载优惠券么?</td>
        </tr>
        <tr>
          <td  class="word4 pa">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
             <td height="28" width="33">&nbsp;</td>
              <td width="400"><input type="radio" name="b7" value="经常查阅商家信息并下载优惠券" /> 经常查阅商家信息并下载优惠券</td>
              <td><input type="radio" name="b7" value="主要查查打折信息什么的，不太用优惠券" /> 主要查查打折信息什么的，不太用优惠券</td>
            </tr>
            <tr>
             <td height="28">&nbsp;</td>
              <td><input type="radio" name="b7" value="偶尔会上上点评网之类的，查一下商家信息" /> 偶尔会上上点评网之类的，查一下商家信息</td>
              <td><input type="radio" name="b7" value="基本不在网上了解这些" /> 基本不在网上了解这些</td>
              </tr>
          </table></td>
        </tr>                           
        
        <tr>
          <td ><a class="a1" id="nextThree" href="javascript:void(0)">下一步</a></td>
        </tr>
      </table>
    </div>
    </td>
  </tr>
  <tr>
    <td height="38" class="center_4">&nbsp;</td>
  </tr>
</table>
</div>
</div>
<script language="JavaScript">
$(document).ready(function(){
    $("#b5_y").click(function(){
        $('input[name=b5_1]').each(function(){
            $(this).attr( "disabled", "");
        });
        $("#b5_2").attr( "disabled", "");        
    })
    
    $("#b5_n").click(function(){
        $('input[name=b5_1]').each(function(){
            $(this).attr( "disabled", "disabled");
            $(this).attr('checked', false);            
        });
        $("#b5_2").attr( "disabled", "disabled"); 
        $("#b5_2").val('');        
    })       
    
    
	$("#nextThree").click(function(){

		var b_1 = $('input:[name=b1]:checked').val();
        var b_2 = $('input:[name=b2]:checked').val();
        var b_3 = $('input:[name=b3]:checked').val();
        var b_4 = $('input:[name=b4]:checked').val();
        var b_5 = $('input:[name=b5]:checked').val();
        var b_5_2 = $('#b5_2').val();
        var b_6 = $('input:[name=b6]:checked').val();
        var b_7 = $('input:[name=b7]:checked').val();        
        

        if(b_1 == undefined || b_1 == ''){
            alert('请回答B1');
            return false;
        }

        if(b_2 == undefined || b_2 == ''){
            alert('请回答B2');
            return false;
        }
        
        if(b_3 == undefined || b_3 == ''){
            alert('请回答B3');
            return false;
        }
        
        if(b_4 == undefined || b_4 == ''){
            alert('请回答B4');
            return false;
        } 
        
        if(b_5 == undefined || b_5 == ''){
            alert('请回答B5');
            return false;
        }
        
        if (b_5 == '是')
        {
            b_5 =  getCheckBox('b5_1');
            
            if(b_5 == undefined || b_5 == ''){
                        alert('请回答B5'); 
                        return false;
            }            
        }
        
        
        
        
        
        if(b_6 == undefined || b_6 == ''){
            alert('请回答B6');
            return false;
        } 
        
        if(b_7 == undefined || b_7 == ''){
            alert('请回答B7');
            return false;
        }                                  

		$.post("stepThree.php?t="+Math.random(),
            {
                b_1:b_1,
                b_2:b_2,
                b_3:b_3,
                b_4:b_4,
                b_5:b_5,
                b_6:b_6,
                b_7:b_7
            },
            function(data){
                var item = eval("("+data+")");
					//alert(item);
                    if (item.result == "INPUT_INVALID") {
                        alert("表单填写不完整！");
                    } else if (item.result == "NONE") {
						alert("会员卡号填写错误！");
						window.location='index.php';
                        return false;
                    } else if (item.result == "EXIST") {
						alert("您已参加过本次调研了！");
						window.location='index.php';
                        return false;
                    } else if (item.result == "LIMIT") {
						alert("感谢您的积极参与，本次活动已结束，如您有任何建议或意见，欢迎发送邮件至research@chongbang.com ");
                        window.location='index.php';
                        return false;
                    } else if (item.result == "OK") {
                        window.location='fourlist.php';
                        return false;
                    } else {
                        window.location='threelist.php';
                        return false;
					}
            }
        );

	});
    
    $("#b5_2").blur(function(){
        var $this   = $(this);
        var other  = $this.val();
        $('input[name=b5_1]').each(function(){
            var $this = $(this);
            if ($this.val().indexOf('其他') != -1)
            {
                $this.val('其他：'+other);
            }
        });
    });
});
</script>
</body>
</html>
