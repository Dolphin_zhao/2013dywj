<?php /* Smarty version Smarty-3.0.4, created on 2013-12-20 17:03:06
         compiled from "/usr/local/www/Daning_new/2013dywj/site/templates/test99/index/questionnaire_4.html" */ ?>
<?php /*%%SmartyHeaderCode:207103297552b407ca2b47c9-72086088%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'be80287351b87879a7b0c9bf4a7d89846b58a1ef' => 
    array (
      0 => '/usr/local/www/Daning_new/2013dywj/site/templates/test99/index/questionnaire_4.html',
      1 => 1387530171,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '207103297552b407ca2b47c9-72086088',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>大宁国际商业广场调研问卷</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<link href="colorbox/colorbox.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
<script>DD_belatedPNG.fix('*'); </script>
<![endif]-->
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="colorbox/jquery.colorbox-min.js"></script>
<script src="js/checkFrom.js" type="text/javascript"></script>
<style type="text/css">
#markDiv { position:fixed; _position:absolute; z-index:9998; left:0; top:0; width:100%; height:100%; _height:1400px; background:#000; opacity:0.7; filter:alpha(opacity=80); -ms-filter:"alpha(opacity=80)"; display:none; }
#dialog_box { position:absolute; z-index:9999; left:50%; top:700px; width:800px; margin:0 0 0 -400px; text-align:center;  text-align:center; display:none;}
</style>
<script type="text/javascript">
$("#markDiv,#dialog_box").hide();
function tvc_show() {
   $("#markDiv,#dialog_box").toggle();
}
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8753756-28']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="main">
<div class="top"><a href="http://www.antinganting.com.cn" target="_blank" title="嘉亭荟"></a></div>
<div class="hd">
<div class="tt5"></div>
<div class="hd1"><img src="css/img/m1.gif" /><img src="css/img/m2.gif" /><img src="css/img/m3_2.gif" /><img src="css/img/m4_1.gif" /></div>
</div>
<div class="center_1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="center_2">
  <tr>
    <td valign="top" class="center_3">
    <div class="center_5">
      <table width="855" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="word1"><span class="word5">D1</span> 您最喜欢的大宁国际商户是?</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="65">餐饮类：</td>
              <td><input type="text" name="d_1_1" size="35" class="iput6" /></td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td>服装类：</td>
              <td><input type="text" name="d_1_2" size="35" class="iput6" /></td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td>其他：</td>
              <td><input type="text" name="d_1_3" size="35" class="iput6" /></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">D2</span> 请问您觉得大宁餐饮还需要哪些种类或品牌？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          	<tr>
              <td width="33" height="28">&nbsp;</td>
              <td><input type="text" name="d_2" size="35" class="iput6" /></td>
            </tr>
          </table></td>
        </tr>
        

        <tr>
          <td class="word1"><span class="word5">D3</span> 您觉得大宁还应该引进哪些服装品牌?</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          	<tr>
              <td width="33" height="28">&nbsp;</td>
              <td><input type="text" name="d_3" size="35" class="iput6" /></td>
            </tr>
          </table></td>
        </tr>       
        
        <tr>
          <td class="word1"><span class="word5">D4</span> 请选择您对大宁最迫切的建议？（1－2项）</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td width="33" height="28">&nbsp;</td>
              <td width="275"><input type="checkbox" name="d_4_1" value="增加餐饮种类" /> 增加餐饮种类</td>
              <td ><input type="checkbox" name="d_4_1" value="增加服装种类" /> 增加服装种类</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="d_4_1" value="增加儿童消费" /> 增加儿童消费</td>
              <td><input type="checkbox" name="d_4_1" value="增加休闲娱乐" /> 增加休闲娱乐</td>
            </tr> 
            <tr>
               <td width="33" height="28">&nbsp;</td>
              <td colspan="2"><input type="checkbox" name="d_4_1" value="改善服务水平" /> 改善服务水平</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td colspan="2"><input type="checkbox" class="d_4_2" name="d_4_1" value="其他建议" /> 具体还有哪些其他建议？  <input type="text" name="d_4_2" size="20" class="iput6" /></td>
            </tr>                                              
          </table></td>
        </tr> 
        
        <tr>
          <td class="word1"><span class="word5">D5</span> 您觉得大宁国际的引导系统是否明确，能快速帮您找到商家位置么？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="105" height="28"><input type="radio" name="d_5" value="做的不错" /> 做的不错</td>
              <td width="185" height="28"><input type="radio" name="d_5" value="一般，有一定引导作用" /> 一般，有一定引导作用</td>
              <td><input type="radio" name="d_5" value="不好，起不到明确的引导作用" /> 不好，起不到明确的引导作用</td>
              </tr>

          </table></td>
        </tr> 
        
        <tr>
          <td class="word1"><span class="word5">D6</span> 请您在以下关键词中，挑出三个您对大宁国际最直接的印象：</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td width="33" height="28">&nbsp;</td>
              <td width="100"><input type="checkbox" name="d_6" value="时尚现代" /> 时尚现代</td>
              <td width="100"><input type="checkbox" name="d_6" value="设施偏旧" /> 设施偏旧</td>
              <td width="115"><input type="checkbox" name="d_6" value="品牌不错" /> 品牌不错</td>
              <td width="100"><input type="checkbox" name="d_6" value="品牌一般" /> 品牌一般</td>
              <td><input type="checkbox" name="d_6" value="环境氛围好" /> 环境氛围好</td>
            </tr>
            <tr>
               <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="d_6" value="环境一般" /> 环境一般</td>
              <td><input type="checkbox" name="d_6" value="比较独特" /> 比较独特</td>
              <td><input type="checkbox" name="d_6" value="没什么特色" /> 没什么特色</td>
              <td><input type="checkbox" name="d_6" value="活动挺多" /> 活动挺多</td>
              <td><input type="checkbox" name="d_6" value="没啥可买的" /> 没啥可买的</td>
            </tr>
            <tr>
               <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="d_6" value="挺好玩的" /> 挺好玩的</td>
              <td><input type="checkbox" name="d_6" value="没新鲜感" /> 没新鲜感</td>
              <td colspan="3"><input type="checkbox" name="d_6" value="离家近，没其他选择" /> 离家近，没其他选择</td>
            </tr>                                                 
          </table></td>
        </tr> 
        
        
        <tr>
          <td class="word1"><span class="word5">D7</span> 周边哪些商业设施您也会经常去?</td>
        </tr>
        <tr>
          <td class="word4 pa1"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="130"><input type="radio" class="d_7_1" name="d_7_1" value="宝山万达广场" /> 宝山万达广场</td>
              <td width="116"><input type="radio" class="d_7_1" name="d_7_1" value="虹口龙之梦" /> 虹口龙之梦</td>
              <td width="120"><input type="radio" class="d_7_1" name="d_7_1" value="闸北大悦城" /> 闸北大悦城</td>
              <td><input type="radio" class="d_7_2" name="d_7_1" value="其他" /> 其他： <input type="text" name="d_7_2" size="20" class="iput6" /></td>
              </tr>
          </table></td>
          </tr>  
        <tr>
        	<td class="word1">您对大宁国际还有什么建议：</td>
        </tr>
        <tr>
          <td class="word4 pa1"><textarea name="d_8" id="d_8" cols="45" rows="5" class="textarea1"></textarea></td>
        </tr>                                               
      </table>
      <table width="821" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><div class="queren">&nbsp;</div></td>
          </tr>
        <tr>
          <td height="100"><a class="a1" id="nextFive" href="javascript:void(0);">下一步</a></td>
        </tr>
      </table>
    </div>
    </td>
  </tr>
  <tr>
    <td height="38" class="center_4">&nbsp;</td>
  </tr>
</table>
</div>
</div>
<!--提交完毕-->
<div id="markDiv"></div>
<div id="dialog_box">
    <div class="tab_1">
        <div class="c1">您已提交完毕，感谢您的配合！</div>
        <div class="pm" align="center"><a href="#" onclick="javascript:window.location='index.php'" class="a3">关闭</a></div>
    </div>
</div>

<script language="JavaScript">
$(document).ready(function(){
	$(".d_merchant_type_class,.d_clothes_type_class").click(function(){
		if (!getCheckBoxLitmit('d_merchant_type', 3) || !getCheckBoxLitmit('d_clothes_type', 3)) {
			alert('最多只能选择三项！');
            return false;
		}
	})
	$('input[name=d_7_2]').attr("disabled","disabled");
	$('input[name=d_4_2]').attr("disabled","disabled");
	
    $('input:[name=d_4_1]:checkbox').click(function(){
        if (!getCheckBoxLitmit('d_4_1', 2)) 
        {
            alert('最多只能选两项');
            return false;
        };    
    });
	
    $('input:[name=d_6]:checkbox').click(function(){
        if (!getCheckBoxLitmit('d_6', 3)) 
        {
            alert('最多只能选三项');
            return false;
        };    
    });

    $('.d_4_2').click(function(){
		if(!$(this).attr("checked")){
			$('input[name=d_4_2]').attr("disabled","disabled");
			$('input[name=d_4_2]').val(""); 			
		}else{
			$('input[name=d_4_2]').attr("disabled","");
		}
    });
	
    $('.d_7_1').click(function(){
        $('input[name=d_7_2]').attr("disabled","disabled");
		$('input[name=d_7_2]').val("");        
    });
    $('.d_7_2').click(function(){
        $('input[name=d_7_2]').attr("disabled","");        
    });	
	
	$("#nextFive").click(function(){
		var d_1_1    = $('input:[name=d_1_1]:text').val();
        var d_1_2    = $('input:[name=d_1_2]:text').val();
        var d_1_3    = $('input:[name=d_1_3]:text').val();

        
        var d_2    = $('input:[name=d_2]:text').val();
	    var d_3    = $('input:[name=d_3]:text').val();

		var d_4_2    = $('input:[name=d_4_2]:text').val();
        var d_4_1    = getCheckBox1('d_4_1');
		
        var d_5    = $('input:[name=d_5]:checked').val();
        var d_6    = getCheckBoxSe('d_6');
        
        var d_7_1    = $('input:[name=d_7_1]:checked').val();
		var d_7_2    = $('input:[name=d_7_2]:text').val();
        
        
        var d_8    = $('textarea:[name=d_8]').val();                                
        
        

		
		if (d_1_1 == undefined || d_1_1 == '' || d_1_2 == undefined || d_1_2 == '' || d_1_3 == undefined || d_1_3 == '') {
			alert('请回答D1');
            return false;
		}

		if (d_2 == undefined || d_2 == '') {
			alert('请回答D2');
            return false;
		}
        
        if (d_3 == undefined || d_3 == '') {
            alert('请回答D3');
            return false;
        } 
        
        if (d_4_1 == undefined || d_4_1 == '') {
            alert('请回答D4');
            return false;
        }
        
        if (d_5 == undefined || d_5 == '') {
            alert('请回答D5');
            return false;
        }
        
        
        if (d_6 == undefined || d_6 == '') {
            alert('请回答D6');
            return false;
        }

		if($('input:[name=d_6]:checked').length != 3){
			alert('请回答D6,挑出三个您对大宁国际最直接的印象');
			return false;
		}
		
        
        if (d_7_1 == undefined || d_7_1 == '') {
            alert('请回答D7');
            return false;
        }else if(d_7_1 == '其他'){
			if(d_7_2 == undefined || d_7_2 == ''){
				alert('请回答D7');
				$('input:[name=d_7_2]').focus();
				return false;			
			}else{
				d_7_1 += ':'+d_7_2;
			}
		}
        
        
        if (d_8 == undefined || d_8 == '') {
            alert('请回填写您对大宁国际的建议');
            return false;
        }                            
		
		function getCheckBox1(inputname)
		{
			var result = new Array();
			var valueList=document.getElementsByName(inputname);
			for (i=0;i<valueList.length;i++){
				if (valueList[i].checked)
				{
					if(valueList[i].value == '其他建议'){
						if(d_4_2 == undefined || d_4_2 == ''){
							$('input:[name=d_4_2]').focus();
							return false;
						}else{
							result.push(valueList[i].value+':'+d_4_2);
						}
					}else{
						result.push(valueList[i].value);
					}
				}
			}
			return result.join("，");	
		}		

		$.post("stepFive.php?t="+Math.random(),
            {
                d_1:'餐饮类：'+d_1_1+'，服装类：'+d_1_2+'，其他：'+d_1_3,
                d_2:d_2,
                d_3:d_3,
                d_4_1:d_4_1,
                d_5:d_5,
                d_6:d_6,
                d_7_1:d_7_1,
                d_8:d_8                
            },
            function(data){
                var item = eval("("+data+")");
					//alert(item);
                    if (item.result == "INPUT_INVALID") {
                        alert("表单填写不完整！");
                    } else if (item.result == "NONE") {
						alert("会员卡号填写错误！");
						window.location='index.php';
                    } else if (item.result == "EXIST") {
						alert("您已参与过此次调研，感谢配合！");
						window.location='index.php';
                    } else if (item.result == "LIMIT") {
						alert("调研已结束，感谢您的积极参与！");
                        window.location='index.php';
                    } else if (item.result == "OK") {
                        $("#markDiv,#dialog_box").toggle();
                    } else {
						window.location='fivelist.php';
					}
            }
        );

	});
});
</script>
</body>
</html>
