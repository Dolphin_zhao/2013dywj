<?php /* Smarty version Smarty-3.0.4, created on 2013-12-24 12:03:26
         compiled from "/usr/local/www/Daning_new/wenjuan/site/templates/test99/index/questionnaire_3.html" */ ?>
<?php /*%%SmartyHeaderCode:174071385952b9078e67d914-56842515%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '030684d7097ac35b07910706b8c24fce170a19b7' => 
    array (
      0 => '/usr/local/www/Daning_new/wenjuan/site/templates/test99/index/questionnaire_3.html',
      1 => 1387856418,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '174071385952b9078e67d914-56842515',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>大宁国际商业广场调研问卷</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.greyText {
    background-color:#DBE5E5;
};
</style>
<!--[if IE 6]>
<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
<script>DD_belatedPNG.fix('*'); </script>
<![endif]-->
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/checkFrom.js" type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8753756-28']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="main">
<div class="top"><a href="http://www.daningdaning.com/cn/" target="_blank" title="大宁国际"></a></div>
<div class="hd">
<div class="tt4"></div>
<div class="hd1"><img src="css/img/m1.gif" /><img src="css/img/m2_2.gif" /><img src="css/img/m3_1.gif" /><img src="css/img/m4.gif" /></div>
</div>
<div class="center_1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="center_2">
  <tr>
    <td valign="top" class="center_3">
    <div class="center_5">
      <table width="855" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="word1"><span class="word5">C1</span>  请问您最喜欢的品牌活动是？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="125"><input type="radio" name="c_1" value="品牌折扣" /> 品牌折扣</td>
              <td width="130"><input type="radio" name="c_1" value="品牌特卖会" /> 品牌特卖会</td>
              <td width="125"><input type="radio" name="c_1" value="餐饮试吃" /> 餐饮试吃</td>
              <td width="165"><input type="radio" name="c_1" value="消费返现及赠券" /> 消费返现及赠券</td>
              <td><input type="radio" name="c_1" value="路演" /> 路演</td>
            </tr>
          </table></td>
          </tr>
          
        <tr>
          <td class="word1"><span class="word5">C2</span> 您希望能参加大宁会员的哪些小型活动？</td>
        </tr>          
          <tr>
          <td class="word4 pa1">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="185"><input type="radio" name="c_2" value="亲子活动及家庭DIY" /> 亲子活动及家庭DIY</td>
              <td width="120"><input type="radio" name="c_2" value="专场特卖" /> 专场特卖</td>
              <td width="120"><input type="radio" name="c_2" value="时尚课堂" /> 时尚课堂</td>
              <td width="135"><input type="radio" name="c_2" value="明星见面会" /> 明星见面会</td>
              <td><input type="radio" name="c_2" value="教育培训" /> 教育培训</td>
            </tr>                                                              
          </table></td>
          </tr>          
          
          
        <tr>
          <td class="word1"><span class="word5">C3</span> 您希望通过何种方式得到活动邀请？</td>
        </tr>
         <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="190"><input type="radio" name="c_3" value="现场宣传并即时参与" /> 现场宣传并即时参与</td>
              <td width="160"><input type="radio" name="c_3" value="会员短信及邮件" /> 会员短信及邮件</td>
              <td width="175"><input type="radio" name="c_3" value="在消费时得到邀请" /> 在消费时得到邀请</td>
              <td><input type="radio" name="c_3" value="商场APP或微信等" /> 商场APP或微信等</td>
            </tr>  
          </table></td>
        </tr>
          
        <tr>
          <td class="word1"><span class="word5">C4</span> 您一般在什么节日会选择外出消费并感受现场气氛？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="100" height="28"><input type="radio" name="c_4" value="圣诞" /> 圣诞</td>
              <td width="100" height="28"><input type="radio" name="c_4" value="春节" /> 春节</td>
              <td width="100" height="28"><input type="radio" name="c_4" value="元旦" /> 元旦 </td>
              <td width="130"><input type="radio" name="c_4" value="六一儿童节" /> 六一儿童节</td>
              <td width="100"><input type="radio" name="c_4" value="情人节" /> 情人节</td>
              <td><input type="radio" name="c_4" value="不一定，根据心情" /> 不一定，根据心情</td>
            </tr>

          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">C5</span> 请问哪项会员卡服务您最感兴趣（1-2项）？</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="176"><input type="checkbox" class="c_5" name="c_5" value="商家折扣" /> 商家折扣</td>
              <td width="200"><input type="checkbox" class="c_5" name="c_5" value="积分换礼" /> 积分换礼</td>
              <td width="195"><input type="checkbox" class="c_5" name="c_5" value="免费停车" /> 免费停车</td>
              <td><input type="checkbox" class="c_5" name="c_5" value="vip会员活动" /> vip会员活动</td>
              </tr>
              <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" class="c_5" name="c_5" value="特卖积分抵扣现金" /> 特卖积分抵扣现金</td>
              <td><input type="checkbox" class="c_5" name="c_5" value="免费参与音乐节等活动" /> 免费参与音乐节等活动</td>
              <td colspan="2"><input type="checkbox" class="c_5" name="c_5" value="会员专享的品牌特卖" /> 会员专享的品牌特卖</td>
              </tr>
          </table></td>
        </tr>
        
        
        <tr>
          <td class="word1"><span class="word5">C6</span> 您最喜欢的积分换礼礼品是？</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="165"><input type="radio" name="c_6" value="食品" /> 食品</td>
              <td width="120"><input type="radio" name="c_6" value="旅游产品" /> 旅游产品</td>
              <td width="170"><input type="radio" name="c_6" value="客户指定商品代购" /> 客户指定商品代购</td>
              <td><input type="radio" name="c_6" value="不关心" /> 不关心</td>
              </tr>
              <tr>
              <td height="28">&nbsp;</td>
              <td><input type="radio" name="c_6" value="大宁专用现金券" /> 大宁专用现金券</td>
              <td><input type="radio" name="c_6" value="数码产品" /> 数码产品</td>
              <td><input type="radio" name="c_6" value="儿童用品及玩具" /> 儿童用品及玩具</td>
              <td><input type="radio" name="c_6" value="化妆品及其他时尚配饰" /> 化妆品及其他时尚配饰</td>
              </tr>
          </table></td>
        </tr>   
        
        
        <tr>
          <td class="word1"><span class="word5">C7</span> 请问作为大宁国际会员，您对大宁会员卡最主要的意见是（1-2项）？</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          	<tr>
               <td width="33" height="28">&nbsp;</td>
              <td><input type="checkbox" class="c_7" name="c_7" value="会员折扣优惠还不错，是我来大宁消费的主要理由之一" /> 会员折扣优惠还不错，是我来大宁消费的主要理由之一</td>
            </tr>
            <tr>
               <td height="28">&nbsp;</td>
              <td><input type="checkbox" class="c_7" name="c_7" value="会员体系，积分制度，服务与活动有一定吸引力" /> 会员体系，积分制度，服务与活动有一定吸引力</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" class="c_7" name="c_7" value="会员服务与优惠较少，积分吸引力不大" /> 会员服务与优惠较少，积分吸引力不大</td>
            </tr> 
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" class="c_7" name="c_7" value="会员权利及优惠等不明晰，积分用处不大，刺激不了我的消费" /> 会员权利及优惠等不明晰，积分用处不大，刺激不了我的消费</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" class="c_7" name="c_7" value="看不出有什么用，有没有无所谓" /> 看不出有什么用，有没有无所谓</td>
            </tr>
          </table></td>
        </tr> 
        <tr>
          <td class="word1"><span class="word5">C8</span> 请问一个可以提供休闲，咖啡饮料，上网，以及便利会员服务的VIP中心对您重要么？</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="215"><input type="radio" name="c_8" value="很重要，早就该有一个了" /> 很重要，早就该有一个了</td>
              <td width="190"><input type="radio" name="c_8" value="一般吧，有了方便点" /> 一般吧，有了方便点</td>
              <td><input type="radio" name="c_8" value="无所谓，有没有都行" /> 无所谓，有没有都行</td>
              </tr>
          </table></td>
        </tr> 
        <tr>
          <td class="word1"><span class="word5">C9</span>  请问您最近一年来，来大宁的次数有什么变化？</td>
        </tr>
         <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="150"><input type="radio" name="c_9" value="比去年少了点" /> 比去年少了点</td>
              <td width="150"><input type="radio" name="c_9" value="和去年差不多吧" /> 和去年差不多吧</td>
              <td><input type="radio" name="c_9" value="比去年来得频繁了" /> 比去年来得频繁了</td>
              </tr>
          </table></td>
        </tr>
        
   
        <tr>
          <td ><a class="a1" id="nextFour" href="javascript:void(0);">下一步</a></td>
        </tr>
      </table>
    </div>
    </td>
  </tr>
  <tr>
    <td height="38" class="center_4">&nbsp;</td>
  </tr>
</table>
</div>
</div>
<script language="JavaScript">
$(document).ready(function(){
    $("#c_10_2, #c_10_3, #c_10_4").hide();    
    
    $("#c10_y").click(function(){
        $('input[name=c_10_1]').each(function(){
            $(this).attr( "disabled", "");
        });
        $("#c_10_2").attr( "disabled", ""); 
        $("#c_10_3").attr( "disabled", "");
        $("#c_10_4").attr( "disabled", "");       
    })
    
    $("#c10_n").click(function(){
        $('input[name=c_10_1]').each(function(){
            $(this).attr( "disabled", "disabled");
            $(this).attr('checked', false);            
        });
        $("#c_10_2").attr( "disabled", "disabled"); 
        $("#c_10_3").attr( "disabled", "disabled");
        $("#c_10_4").attr( "disabled", "disabled");
        $("#c_10_2").val('');  
        $("#c_10_3").val('');  
        $("#c_10_4").val(''); 
        $("#c_10_2, #c_10_3, #c_10_4").hide();                     
    })  
        
    $('.c_5, .c_7').click(function(){
        if (!getCheckBoxLitmit('c_5', 2) || !getCheckBoxLitmit('c_7', 2)) 
        {
            alert('最多只能选两项');
            return false;
        };    
    });
    
    $('.c_12, .c_13').click(function(){
        if (!getCheckBoxLitmit('c_12', 2) || !getCheckBoxLitmit('c_13', 2)) 
        {
            alert('最多只能选两项');
            return false;
        };    
    });
    
    $('.c_10_1').click(function(){
        if (!getCheckBoxLitmit('c_10_1', 3)) 
        {
            alert('最多只能选三项');
            return false;
        };
        
        if (getCheckBoxLitmit1('c_10_1') == 0)
        {
            $("#c_10_2").hide();
            $("#c_10_2").val('');
            $("#c_10_3").hide();
            $("#c_10_3").val('');
            $("#c_10_4").hide();
            $("#c_10_4").val('');            
        }
        
        if (getCheckBoxLitmit1('c_10_1') == 1)
        {
            if($("#c_10_2").is(':visible') == false)
            {
                $("#c_10_2").show();                
            }
                $("#c_10_3").hide();
                $("#c_10_3").val('');
                $("#c_10_4").hide();
                $("#c_10_4").val('');
           
        }
        
        if (getCheckBoxLitmit1('c_10_1') == 2)
        {
            if($("#c_10_2").is(':visible') == false)
            {
 
                $("#c_10_2").show();                
            }
            if($("#c_10_3").is(':visible') == false)
            {

                $("#c_10_3").show();                
            }
            
            $("#c_10_4").hide();
            $("#c_10_4").val('');               
        } 
        
        if (getCheckBoxLitmit1('c_10_1') == 3)
        {
            $("#c_10_2").show();
            $("#c_10_3").show();
            $("#c_10_4").show();
        }        
        

            
    });    
    
    $("#nextFour").click(function()
    {
    var c_1 = $('input:[name=c_1]:checked').val();
    var c_2 = $('input:[name=c_2]:checked').val();
        
    var c_3 = $('input:[name=c_3]:checked').val();
    var c_4 = $('input:[name=c_4]:checked').val();
	var c_5 = getCheckBoxSe('c_5');
    var c_6 = $('input:[name=c_6]:checked').val();
	var c_7 = getCheckBoxSe('c_7');
    var c_8 = $('input:[name=c_8]:checked').val();
    var c_9  = $('input:[name=c_9]:checked').val();
    
    if(c_1 == undefined || c_1 == ''){
            alert('请回答C1');
            return false;
    }
    
    if(c_2 == undefined || c_2 == ''){
            alert('请回答C2');
            return false;
    }                              
    
    if(c_3 == undefined || c_3 == ''){
            alert('请回答C3');
            return false;
    }
    
    if(c_4 == undefined || c_4 == ''){
            alert('请回答C4');
            return false;
    }  

    if(c_5 == undefined || c_5 == ''){
            alert('请回答C5');
            return false;
    } 
    
    if(c_6 == undefined || c_6 == ''){
            alert('请回答C6');
            return false;
    }
    
    if(c_7 == undefined || c_7 == ''){
            alert('请回答C7');
            return false;
    }  
    
    if(c_8 == undefined || c_8 == ''){
            alert('请回答C8');
            return false;
    }                    
                                         
    if(c_9 == undefined || c_9 == ''){
            alert('请回答C9');
            return false;
    }
    


        $.post("stepFour.php?t="+Math.random(),
            {
                c_1:c_1,
                c_2:c_2,
                c_3:c_3,
                c_4:c_4,
                c_5:c_5,
                c_6:c_6,
                c_7:c_7,
                c_8:c_8,
                c_9:c_9              
            },
            function(data){
                var item = eval("("+data+")");
                    if (item.result == "INPUT_INVALID") {
                        alert("表单填写不完整！");
                    } else if (item.result == "NONE") {
                        alert("会员卡号填写错误！");
                        window.location='index.php';
                    } else if (item.result == "EXIST") {
                        alert("您已参与过此次调研，感谢配合！");
                        window.location='index.php';
                    } else if (item.result == "LIMIT") {
                        alert("调研已结束，感谢您的积极参与！");
                        window.location='index.php';
                    } else if (item.result == "OK") {
                        window.location='fivelist.php';
                    } else {
                        window.location='fourlist.php';
                    }
            });    
    });              
});
</script>
</body>
</html>


