<?php /* Smarty version Smarty-3.0.4, created on 2013-12-24 12:03:07
         compiled from "/usr/local/www/Daning_new/wenjuan/site/templates/test99/index/questionnaire_2.html" */ ?>
<?php /*%%SmartyHeaderCode:118132791652b9077bd3ebf3-83539069%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c71e5860435f430b0fe56e3a39cb10849d12b516' => 
    array (
      0 => '/usr/local/www/Daning_new/wenjuan/site/templates/test99/index/questionnaire_2.html',
      1 => 1387856423,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '118132791652b9077bd3ebf3-83539069',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>大宁国际商业广场调研问卷</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
<script>DD_belatedPNG.fix('*'); </script>
<![endif]-->
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/checkFrom.js" type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8753756-28']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="main">
<div class="top"><a href="http://www.daningdaning.com/cn/" target="_blank" title="大宁国际"></a></div>
<div class="hd">
<div class="tt3"></div>
<div class="hd1"><img src="css/img/m1_2.gif" /><img src="css/img/m2_1.gif" /><img src="css/img/m3.gif" /><img src="css/img/m4.gif" /></div>
</div>
<div class="center_1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="center_2">
  <tr>
    <td valign="top" class="center_3">
    <div class="center_5">
      <table width="855" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="word1"><span class="word5">B1</span> 请问您一般通过什么样的交通工具来大宁国际？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="95"><input type="radio" name="b1" value="自驾车" /> 自驾车</td>
              <td width="95"><input type="radio" name="b1" value="出租车" /> 出租车</td>
              <td width="75"><input type="radio" name="b1" value="地铁" /> 地铁</td>
              <td width="95"><input type="radio" name="b1" value="公交车" /> 公交车</td>
              <td width="95"><input type="radio" name="b1" value="自行车" /> 自行车</td>
              <td><input type="radio" name="b1" value="步行" /> 步行</td>
              </tr>
          </table></td>
          </tr>
        <tr>
          <td class="word1"><span class="word5">B2</span> 请问您来大宁国际一般花费多少交通时间？ </td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
         <td width="33">&nbsp;</td>
              <td width="120"><input type="radio" name="b2" value="10分钟以内" /> 10分钟以内</td>
              <td width="120"><input type="radio" name="b2" value="10－20分钟" /> 10－20分钟</td>
              <td width="120"><input type="radio" name="b2" value="20－30分钟" /> 20－30分钟</td>
              <td width="120"><input type="radio" name="b2" value="30－60分钟" /> 30－60分钟</td>
              <td><input type="radio" name="b2" value="60分钟以上" /> 60分钟以上</td>
              </tr>

          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">B3</span> 您是否会特意因为活动（大宁音乐季，圣诞活动等）来大宁？</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="95"><input type="radio" name="b3" value="经常会" /> 经常会</td>
              <td width="175"><input type="radio" name="b3" value="不一定，看活动内容" /> 不一定，看活动内容</td>
              <td width="95"><input type="radio" name="b3" value="很少会" /> 很少会</td>
              <td><input type="radio" name="b3" value="不会" /> 不会</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">B4</span> 请问您知道或参加过下列任何大宁国际的推广（广场）活动么？</td>
        </tr> 
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="160"><input type="radio" value="有" name="b4" class="myhave c_join_what_select" id="b4_y" /> 有，请选择 </td>
              <td><input type="radio" value="没有" name="b4" class="mynone c_join_what_select" id="b4_n" /> 没有，跳过</td>
            </tr>
            </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td width="33" height="30">&nbsp;</td>
              <td colspan="2">请选择您最喜欢的推广活动（最多3项）</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td width="335"><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="蛇舞新春 春节活动" /> 蛇舞新春 春节活动</td>
              <td><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="李代沫演唱会" /> 李代沫演唱会</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="何韵诗签唱会" /> 何韵诗签唱会</td>
              <td><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="中央广场花园 美国花卉大师鲜花服饰秀" /> 中央广场花园 美国花卉大师鲜花服饰秀</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="六一儿童节 Jelly Belly 糖豆世界" /> 六一儿童节 Jelly Belly 糖豆世界  </td>
              <td><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="西班牙语日 弗拉门戈舞蹈秀" /> 西班牙语日 弗拉门戈舞蹈秀</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td colspan="3"><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="荷兰艺术家Marijie装置展 激光开幕秀" /> 荷兰艺术家Marijie装置展 激光开幕秀</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="徐佳莹仲夏音乐节" /> 徐佳莹仲夏音乐节</td>
              <td><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="阿卡贝拉音乐会 台湾O－Kai合唱团" /> 阿卡贝拉音乐会 台湾O－Kai合唱团</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="上海旅游节花车巡游" /> 上海旅游节花车巡游</td>
              <td><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="圣诞新年 乐林 悦圣诞  圣诞互动等" /> 圣诞新年 乐林 悦圣诞  圣诞互动等</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td colspan="3"><input type="checkbox" name="b4_1" class="c_join_what_class haveaction" value="大宁音乐季 音乐嘉年华（转角音乐会，何韵诗，江美琪，李泉，果味VC）" /> 大宁音乐季 音乐嘉年华（转角音乐会，何韵诗，江美琪，李泉，果味VC）</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">B5</span> 请问您对大宁国际的打折优惠活动满意么?</td>
        </tr>
        <tr>
          <td  class="word4 pa">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
             <td height="28" width="33">&nbsp;</td>
              <td width="326"><input type="radio" name="b5" value="满意，打折优惠挺多" /> 满意，打折优惠挺多</td>
              <td><input type="radio" name="b5" value="还行吧，最好能再多点折扣活动" /> 还行吧，最好能再多点折扣活动</td>
            </tr>
            <tr>
             <td height="28">&nbsp;</td>
              <td><input type="radio" name="b5" value="一般，打折优惠活动较少" /> 一般，打折优惠活动较少</td>
              <td><input type="radio" name="b5" value="很不满意，比其他地方差的远了" /> 很不满意，比其他地方差的远了</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">B6</span> 请问您会为打折或特卖信息特意来大宁国际消费么？</td>
        </tr>
        <tr>
          <td  class="word4 pa">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td height="28" width="33">&nbsp;</td>
              <td width="95"><input type="radio" name="b6" value="一般会" /> 一般会</td>
              <td width="160"><input type="radio" name="b6" value="有时会，看品牌吧" /> 有时会，看品牌吧</td>
              <td width="185"><input type="radio" name="b6" value="没所谓，遇见了就买点" /> 没所谓，遇见了就买点</td>
              <td><input type="radio" name="b6" value="不关心" /> 不关心</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">B7</span> 请问打折与促销对您在哪方面的消费影响较大？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="75"><input type="radio" name="b7" value="餐饮" /> 餐饮</td>
              <td width="75"><input type="radio" name="b7" value="服装" /> 服装</td>
              <td width="75"><input type="radio" name="b7" value="大卖场" /> 大卖场</td>
              <td width="205"><input type="radio" name="b7" value="生活服务（美容美发等）" /> 生活服务（美容美发等）</td>
              <td><input type="radio" name="b7" value=" 家用电器" /> 家用电器</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">B8</span> 请问您一般从哪里得知大宁国际的折扣及活动信息?</td>
        </tr>
        <tr>
          <td  class="word4 pa">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
             <td height="28" width="33">&nbsp;</td>
              <td width="108"><input type="radio" name="b8" value="电视电台" /> 电视电台</td>
              <td width="150"><input type="radio" name="b8" value="报刊杂志" /> 报刊杂志</td>
              <td width="165"><input type="radio" name="b8" value="现场及广场内广告" /> 现场及广场内广告</td>
              <td><input type="radio" name="b8" value="报刊杂志" /> 报刊杂志</td>
            </tr>
            <tr>
             <tr>
             <td height="28">&nbsp;</td>
              <td width="108"><input type="radio" name="b8" value="商户宣传" /> 商户宣传</td>
              <td width="150"><input type="radio" name="b8" value="询问处及宣传册" /> 询问处及宣传册</td>
              <td width="165"><input type="radio" name="b8" value="商场APP及微信" /> 商场APP及微信</td>
              <td><input type="radio" name="b8" value="微博及网络" /> 微博及网络</td>
            </tr>
          </table></td>
        </tr>                           
        <tr>
          <td class="word1"><span class="word5">B9</span> 您是否使用大宁国际APP电子会员卡？</td>
        </tr> 
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="95"><input type="radio" value="有" name="b9" class="myhave c_join_what_select" id="b9_y" /> 有 </td>
              <td><input type="radio" value="没有" name="b9" class="mynone c_join_what_select" id="b9_n" /> 没有（跳过本题）</td>
            </tr>
            </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td width="33" height="30">&nbsp;</td>
              <td colspan="5">请问您对哪项电子会员卡的服务最感兴趣？（最多3项）</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td width="105"><input type="checkbox" name="b9_1" class="c_join_what_class haveaction" value="积分查询" /> 积分查询</td>
              <td width="135"><input type="checkbox" name="b9_1" class="c_join_what_class haveaction" value="比实体卡方便" /> 比实体卡方便</td>
              <td width="105"><input type="checkbox" name="b9_1" class="c_join_what_class haveaction" value="独享福利" /> 独享福利</td>
              <td width="135"><input type="checkbox" name="b9_1" class="c_join_what_class haveaction" value="会员活动报名" /> 会员活动报名</td>
              <td><input type="checkbox" name="b9_1" class="c_join_what_class haveaction" value="大宁游戏礼品" /> 大宁游戏礼品</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td ><a class="a1" id="nextThree" href="javascript:void(0);">下一步</a></td>
        </tr>
      </table>
    </div>
    </td>
  </tr>
  <tr>
    <td height="38" class="center_4">&nbsp;</td>
  </tr>
</table>
</div>
</div>
<script language="JavaScript">
$(document).ready(function(){
    $("#b4_y").click(function(){
        $('input[name=b4_1]').each(function(){
            $(this).attr( "disabled", "");
        });      
    })
    
    $("#b4_n").click(function(){
        $('input[name=b4_1]').each(function(){
            $(this).attr( "disabled", "disabled");
            $(this).attr('checked', false);            
        });     
    })
    $("#b9_y").click(function(){
        $('input[name=b9_1]').each(function(){
            $(this).attr( "disabled", "");
        });      
    })
    
    $("#b9_n").click(function(){
        $('input[name=b9_1]').each(function(){
            $(this).attr( "disabled", "disabled");
            $(this).attr('checked', false);            
        });     
    })	
    $('.haveaction').click(function(){
        if (!getCheckBoxLitmit('b9_1', 3) || !getCheckBoxLitmit('b4_1', 3)) 
        {
            alert('最多只能选三项');
            return false;
        };    
    });    
    
	$("#nextThree").click(function(){

		var b_1 = $('input:[name=b1]:checked').val();
        var b_2 = $('input:[name=b2]:checked').val();
        var b_3 = $('input:[name=b3]:checked').val();
        var b_4 = $('input:[name=b4]:checked').val();
        var b_5 = $('input:[name=b5]:checked').val();
        var b_6 = $('input:[name=b6]:checked').val();
        var b_7 = $('input:[name=b7]:checked').val();
		var b_8 = $('input:[name=b8]:checked').val();
		var b_9 = $('input:[name=b9]:checked').val();
        

        if(b_1 == undefined || b_1 == ''){
            alert('请回答B1');
            return false;
        }

        if(b_2 == undefined || b_2 == ''){
            alert('请回答B2');
            return false;
        }
        
        if(b_3 == undefined || b_3 == ''){
            alert('请回答B3');
            return false;
        }
        
        if(b_4 == undefined || b_4 == ''){
            alert('请回答B4');
            return false;
        }
        if (b_4 == '有')
        {
            b_4 =  getCheckBoxSe('b4_1');
            
            if(b_4 == undefined || b_4 == ''){
                        alert('请回答B4'); 
                        return false;
            }else{
				b_4 ='有:'+b_4;
			}            
        }		
        
        if(b_5 == undefined || b_5 == ''){
            alert('请回答B5');
            return false;
        }
        if(b_6 == undefined || b_6 == ''){
            alert('请回答B6');
            return false;
        }
        if(b_7 == undefined || b_7 == ''){
            alert('请回答B7');
            return false;
        }
        if(b_8 == undefined || b_8 == ''){
            alert('请回答B8');
            return false;
        }
        if(b_9 == undefined || b_9 == ''){
            alert('请回答B9');
            return false;
        }		
        

        if (b_9 == '有')
        {
            b_9 =  getCheckBoxSe('b9_1');
            
            if(b_9 == undefined || b_9 == ''){
                        alert('请回答B9'); 
                        return false;
            }else{
				b_9 ='有:'+b_9;
			}            
        }        
        
    
	

		$.post("stepThree.php?t="+Math.random(),
            {
                b_1:b_1,
                b_2:b_2,
                b_3:b_3,
                b_4:b_4,
                b_5:b_5,
                b_6:b_6,
                b_7:b_7,
				b_8:b_8,
				b_9:b_9
            },
            function(data){
                var item = eval("("+data+")");
					//alert(item);
                    if (item.result == "INPUT_INVALID") {
                        alert("表单填写不完整！");
                    } else if (item.result == "NONE") {
						alert("会员卡号填写错误！");
						window.location='index.php';
                    } else if (item.result == "EXIST") {
						alert("您已参与过此次调研，感谢配合！");
						window.location='index.php';
                    } else if (item.result == "LIMIT") {
						alert("调研已结束，感谢您的积极参与！");
                        window.location='index.php';
                    } else if (item.result == "OK") {
                        window.location='fourlist.php';
                    } else {
                        window.location='threelist.php';
					}
            }
        );

	});
    
    $("#b5_2").blur(function(){
        var $this   = $(this);
        var other  = $this.val();
        $('input[name=b5_1]').each(function(){
            var $this = $(this);
            if ($this.val().indexOf('其他') != -1)
            {
                $this.val('其他：'+other);
            }
        });
    });
});
</script>
</body>
</html>

