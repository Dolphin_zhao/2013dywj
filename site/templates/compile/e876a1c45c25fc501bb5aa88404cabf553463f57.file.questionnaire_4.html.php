<?php /* Smarty version Smarty-3.0.4, created on 2013-01-04 17:04:44
         compiled from "/usr/local/www/Daning_new/2012dywj/site/templates/test99/index/questionnaire_4.html" */ ?>
<?php /*%%SmartyHeaderCode:97493907450e69b2c1f0b40-92305051%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e876a1c45c25fc501bb5aa88404cabf553463f57' => 
    array (
      0 => '/usr/local/www/Daning_new/2012dywj/site/templates/test99/index/questionnaire_4.html',
      1 => 1357289460,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '97493907450e69b2c1f0b40-92305051',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>大宁国际商业广场调研问卷</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
<script>DD_belatedPNG.fix('*'); </script>
<![endif]-->
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/checkFrom.js" type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8753756-28']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="main">
<div class="top"><a href="http://www.antinganting.com.cn" target="_blank" title="嘉亭荟"></a></div>
<div class="hd">
<div class="tt5"></div>
<div class="hd1"><img src="css/img/m1.gif" /><img src="css/img/m2.gif" /><img src="css/img/m3_2.gif" /><img src="css/img/m4_1.gif" /><img src="css/img/m5.gif" /></div>
</div>
<div class="center_1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="center_2">
  <tr>
    <td valign="top" class="center_3">
    <div class="center_5">
      <table width="855" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="word1"><span class="word5">D1</span> 以下餐饮的消费频率?</td>
        </tr>
        <tr>
          <td class="word4 pa1">
          <table width="665" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="152" height="52" class="line1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;餐饮种类</td>
              <td width="98" align="center" class="line1" nowrap>经常去<br />
                <span class="word6">(每月两次以上)</td>
              <td width="125" align="center" class="line1" nowrap>偶尔去<br />
                <span class="word6">(每月不到一次)</span></td>
              <td width="129" align="center" class="line2" nowrap>不太去</td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3" nowrap>　　西式快餐类（KFC，汉堡王，必胜客等）</td>
              <td height="28" align="center" class="line3" nowrap><input type="radio" name="d_1_1" value="西式快餐类（KFC，汉堡王，必胜客等）：经常去" /></td>
              <td height="28" align="center" class="line3" nowrap><input type="radio" name="d_1_1" value="西式快餐类（KFC，汉堡王，必胜客等）：偶尔去" /></td>
              <td height="28" align="center" class="line4" nowrap><input type="radio" name="d_1_1" value="西式快餐类（KFC，汉堡王，必胜客等）：不太去" />
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　中式快餐类（一茶一坐，大家乐等）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_2" value="中式快餐类（一茶一坐，大家乐等）：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_2" value="中式快餐类（一茶一坐，大家乐等）：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_1_2" value="中式快餐类（一茶一坐，大家乐等）：不太去" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　川湘菜类（望湘园，旺池等）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_3" value="川湘菜类（望湘园，旺池等）：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_3" value="川湘菜类（望湘园，旺池等）：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_1_3" value="川湘菜类（望湘园，旺池等）：不太去" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　粤菜及茶餐厅（吉望，避风塘等）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_4" value="粤菜及茶餐厅（吉望，避风塘）：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_4" value="粤菜及茶餐厅（吉望，避风塘）：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_1_4" value="粤菜及茶餐厅（吉望，避风塘）：不太去" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　日本韩国料理（釜山料理，伊秀等）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_5" value="日本韩国料理（釜山料理，伊秀等）：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_5" value="日本韩国料理（釜山料理，伊秀等）：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_1_5" value="日本韩国料理（釜山料理，伊秀等）：不太去" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　其他中餐（西贝，古丽仙等）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_6" value="其他中餐（西贝，古丽仙等）：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_6" value="其他中餐（西贝，古丽仙等）：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_1_6" value="其他中餐（西贝，古丽仙等）：不太去" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　西点面包（巴黎贝甜，面包新语）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_7" value="西点面包（巴黎贝甜，面包新语）：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_7" value="西点面包（巴黎贝甜，面包新语）：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_1_7" value="西点面包（巴黎贝甜，面包新语）：不太去" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　咖啡冷饮（星巴克，哈根达斯，DQ）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_8" value="咖啡冷饮（星巴克，哈根达斯，DQ）：经常去" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_1_8" value="咖啡冷饮（星巴克，哈根达斯，DQ）：偶尔去" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_1_8" value="咖啡冷饮（星巴克，哈根达斯，DQ）：不太去" /></td>
            </tr>
          
          </table></td>
          </tr>
        <tr>
          <td class="word1"><span class="word5">D2</span> 请问您平均每月在大宁国际用餐几次？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="100" height="28"><input type="radio" name="d_2" value="不到1次" /> 不到1次</td>
              <td width="116" height="28"><input type="radio" name="d_2" value="1-2次" /> 1-2次</td>
              <td width="126" height="28"><input type="radio" name="d_2" value="3-4次" /> 3-4次</td>
              <td width="126"><input type="radio" name="d_2" value="5-6次" />  5-6次</td>
              <td><input type="radio" name="d_2" value="7次以上" />  7次以上</td>
              </tr>

          </table></td>
        </tr>
        

        <tr>
          <td class="word1"><span class="word5">D3</span> 请问您对大宁国际餐饮方面的印象是?</td>
        </tr>
        <tr>
          <td class="word4 pa1"><table width="537" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="152" height="52" class="line1"></td>
              <td width="98" align="center" class="line1">满意</td>
              <td width="125" align="center" class="line1">一般</td>
              <td width="129" align="center" class="line2">不满意</td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　品种</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_3_1" value="品种：满意" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_3_1" value="品种：一般" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_3_1" value="品种：不满意" />
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　档次</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_3_2" value="档次：满意" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_3_2" value="档次：一般" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_3_2" value="档次：不满意" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　环境</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_3_3" value="环境：满意" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_3_3" value="环境：一般" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_3_3" value="环境：不满意" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　价格</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_3_4" value="价格：满意" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_3_4" value="价格：一般" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_3_4" value="价格：不满意" /></td>
            </tr>
         
          </table></td>
          </tr>        
        
        
        
        
        
        <tr>
          <td class="word1"><span class="word5">D4</span> 请问您觉得大宁餐饮还需要哪些种类或品牌？</td>
        </tr>
        <tr>
          <td  class="word4 pa">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <textarea cols="60" rows="6" name="d_4"></textarea>
          </td>
        </tr>
        
        <tr>
          <td class="word1"><span class="word5">D5</span> 请问您在大宁国际单次选购服装一般的消费是？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="100" height="28"><input type="radio" name="d_5" value="100元以内" /> 100元以内</td>
              <td width="116" height="28"><input type="radio" name="d_5" value="100-200元" /> 100-200元</td>
              <td width="126" height="28"><input type="radio" name="d_5" value="200-500元" /> 200-500元</td>
              <td width="126"><input type="radio" name="d_5" value="500-1000元" />  500-1000元</td>
              <td><input type="radio" name="d_5" value="1000元以上" />  1000元以上</td>
              </tr>

          </table></td>
        </tr> 
        
        <tr>
          <td class="word1"><span class="word5">D6</span> 请问您及您的家庭在大宁购买服装的情况是：</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td width="33" height="28">&nbsp;</td>
              <td><input type="radio" name="d_6" value="大部分衣服都在大宁买" /> 大部分衣服都在大宁买</td>
            </tr>
            <tr>
               <td height="28">&nbsp;</td>
              <td ><input type="radio" name="d_6" value="小部分服装会在大宁买" /> 小部分服装会在大宁买</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="radio" name="d_6" value="仅有很少的衣服在大宁买" /> 仅有很少的衣服在大宁买</td>
            </tr> 
            <tr>
              <td height="28">&nbsp;</td>
              <td><input type="radio" name="d_6" value="基本不在大宁买衣服" /> 基本不在大宁买衣服</td>
            </tr>                                               
          </table></td>
        </tr> 
        
        
        <tr>
          <td class="word1"><span class="word5">D7</span> 请问您在大宁购买不同服装的情况?</td>
        </tr>
        <tr>
          <td class="word4 pa1"><table width="681" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="152" height="52" class="line1"></td>
              <td width="98" align="center" class="line1">经常买</td>
              <td width="125" align="center" class="line1">偶尔买买</td>
              <td width="129" align="center" class="line2">基本不买</td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3" nowrap>　　快时尚（优衣库，C&A等）</td>
              <td height="28" align="center" class="line3" nowrap><input type="radio" name="d_7_1" value="快时尚（优衣库，C&A等）：经常买" /></td>
              <td height="28" align="center" class="line3" nowrap><input type="radio" name="d_7_1" value="快时尚（优衣库，C&A等）：偶尔买买" /></td>
              <td height="28" align="center" class="line4" nowrap><input type="radio" name="d_7_1" value="快时尚（优衣库，C&A等）：基本不买" />
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3" nowrap>　　休闲女装（ELAND， Esprit， Ochirly等）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_7_2" value="休闲女装（ELAND， Esprit， Ochirly等）：经常买" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_7_2" value="休闲女装（ELAND， Esprit， Ochirly等）：偶尔买买" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_7_2" value="休闲女装（ELAND， Esprit， Ochirly等）：基本不买" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3" nowrap>　　休闲男装（GXG，CABBEN URBAN等）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_7_3" value="休闲男装（GXG，CABBEN URBAN等）：经常买" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_7_3" value="休闲男装（GXG，CABBEN URBAN等）：偶尔买买" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_7_3" value="休闲男装（GXG，CABBEN URBAN等）：基本不买" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3" nowrap>　　鞋帽类（INBASE, 热风等）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_7_4" value="鞋帽类（INBASE, 热风等）：经常买" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_7_4" value="鞋帽类（INBASE, 热风等）：偶尔买买" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_7_4" value="鞋帽类（INBASE, 热风等）：基本不买" /></td>
            </tr>
            
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3" nowrap>　　运动时尚 （SPORT 100等）</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_7_5" value="运动时尚 （SPORT 100等）：经常买" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_7_5" value="运动时尚 （SPORT 100等）：偶尔买买" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_7_5" value="运动时尚 （SPORT 100等）：基本不买" /></td>
            </tr>            
         
          </table></td>
          </tr>  
          
          
  <tr>
          <td class="word1"><span class="word5">D8</span> 请问您对大宁国际的服装（包括鞋帽）零售主要有什么印象?</td>
        </tr>
        <tr>
          <td class="word4 pa1"><table width="537" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="152" height="52" class="line1"></td>
              <td width="98" align="center" class="line1">满意</td>
              <td width="125" align="center" class="line1">一般</td>
              <td width="129" align="center" class="line2">不满意</td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　品牌</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_8_1" value="品牌：满意" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_8_1" value="品牌：一般" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_8_1" value="品牌：不满意" />
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　档次</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_8_2" value="档次：满意" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_8_2" value="档次：一般" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_8_2" value="档次：不满意" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28" class="line3">　　价格</td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_8_3" value="价格：满意" /></td>
              <td height="28" align="center" class="line3"><input type="radio" name="d_8_3" value="价格：一般" /></td>
              <td height="28" align="center" class="line4"><input type="radio" name="d_8_3" value="价格：不满意" /></td>
            </tr>
         
          </table></td>
          </tr> 
          
        <tr>
          <td class="word1"><span class="word5">D9</span> 您觉得大宁还应该引进哪些服装品牌？</td>
        </tr>
        <tr>
          <td  class="word4 pa">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
           <textarea cols="60" rows="6" name="d_9"></textarea>
          </td>
        </tr>                                                

        <tr>
          <td><a class="a1" id="nextFive" href="javascript:void(0)">下一步</a></td>
        </tr>
      </table>
    </div>
    </td>
  </tr>
  <tr>
    <td height="38" class="center_4">&nbsp;</td>
  </tr>
</table>
</div>
</div>
<script language="JavaScript">
$(document).ready(function(){
	$(".d_merchant_type_class,.d_clothes_type_class").click(function(){
		if (!getCheckBoxLitmit('d_merchant_type', 3) || !getCheckBoxLitmit('d_clothes_type', 3)) {
			alert('最多只能选择三项！');
            return false;
		}
	})

	$("#nextFive").click(function(){
		var d_1_1    = $('input:[name=d_1_1]:checked').val();
        var d_1_2    = $('input:[name=d_1_2]:checked').val();
        var d_1_3    = $('input:[name=d_1_3]:checked').val();
        var d_1_4    = $('input:[name=d_1_4]:checked').val();
        var d_1_5    = $('input:[name=d_1_5]:checked').val();
        var d_1_6    = $('input:[name=d_1_6]:checked').val();
        var d_1_7    = $('input:[name=d_1_7]:checked').val();
        var d_1_8    = $('input:[name=d_1_8]:checked').val(); 
        
        var d_2    = $('input:[name=d_2]:checked').val();
        var d_3_1    = $('input:[name=d_3_1]:checked').val();
        var d_3_2    = $('input:[name=d_3_2]:checked').val();
        var d_3_3    = $('input:[name=d_3_3]:checked').val();
        var d_3_4    = $('input:[name=d_3_4]:checked').val();
        
        var d_4    = $('textarea:[name=d_4]').val();
        var d_5    = $('input:[name=d_5]:checked').val();
        var d_6    = $('input:[name=d_6]:checked').val();
        
        var d_7_1    = $('input:[name=d_7_1]:checked').val();
        var d_7_2    = $('input:[name=d_7_2]:checked').val();
        var d_7_3    = $('input:[name=d_7_3]:checked').val();
        var d_7_4    = $('input:[name=d_7_4]:checked').val(); 
        var d_7_5    = $('input:[name=d_7_5]:checked').val();
        
        var d_8_1    = $('input:[name=d_8_1]:checked').val();
        var d_8_2    = $('input:[name=d_8_2]:checked').val();
        var d_8_3    = $('input:[name=d_8_3]:checked').val();
        
        var d_9    = $('textarea:[name=d_9]').val();                                
        
        

		if (d_1_1 == undefined || d_1_1 == '' || d_1_2 == undefined || d_1_2 == '' || d_1_3 == undefined || d_1_3 == '' || d_1_4 == undefined || d_1_4 == '' || d_1_5 == undefined || d_1_5 == '' || d_1_6 == undefined || d_1_6 == '' || d_1_7 == undefined || d_1_7 == '' || d_1_8 == undefined || d_1_8 == '') {
			alert('请回答D1');
            return false;
		}

		if (d_2 == undefined || d_2 == '') {
			alert('请回答D2');
            return false;
		}
        
        if (d_3_1 == undefined || d_3_1 == '' || d_3_2 == undefined || d_3_2 == '' || d_3_3 == undefined || d_3_3 == '' || d_3_4 == undefined || d_3_4 == '') {
            alert('请回答D3');
            return false;
        } 
        
        if (d_4 == undefined || d_4 == '') {
            alert('请回答D4');
            $('textarea:[name=d_4]').focus();
            return false;
        }
        
        if (d_5 == undefined || d_5 == '') {
            alert('请回答D5');
            return false;
        }
        
        
        if (d_6 == undefined || d_6 == '') {
            alert('请回答D6');
            return false;
        } 
        
        if (d_7_1 == undefined || d_7_1 == '' || d_7_2 == undefined || d_7_2 == '' || d_7_3 == undefined || d_7_3 == '' || d_7_4 == undefined || d_7_4 == '' || d_7_5 == undefined || d_7_5 == '') {
            alert('请回答D7');
            return false;
        }
        
        
        if (d_8_1 == undefined || d_8_1 == '' || d_8_2 == undefined || d_8_2 == '' || d_8_3 == undefined || d_8_3 == '') {
            alert('请回答D8');
            return false;
        }                 
                                     
         if (d_9 == undefined || d_9 == '') {
            alert('请回答D9');
             $('textarea:[name=d_9]').focus();
            return false;
        }              

		

		$.post("stepFive.php?t="+Math.random(),
            {
                d_1_1:d_1_1,
                d_1_2:d_1_2,
                d_1_3:d_1_3,
                d_1_4:d_1_4,
                d_1_5:d_1_5,
                d_1_6:d_1_6,
                d_1_7:d_1_7,
                d_1_8:d_1_8,
                d_2:d_2,
                d_3_1:d_3_1,
                d_3_2:d_3_2,
                d_3_3:d_3_3,
                d_3_4:d_3_4,
                d_4:d_4,
                d_5:d_5,
                d_6:d_6,
                d_7_1:d_7_1,
                d_7_2:d_7_2,
                d_7_3:d_7_3,
                d_7_4:d_7_4,
                d_7_5:d_7_5,
                d_8_1:d_8_1,
                d_8_2:d_8_2,
                d_8_3:d_8_3,
                d_9:d_9                
            },
            function(data){
                var item = eval("("+data+")");
					//alert(item);
                    if (item.result == "INPUT_INVALID") {
                        alert("表单填写不完整！");
                    } else if (item.result == "NONE") {
						alert("会员卡号填写错误！");
						window.location='index.php';
                        return false;
                    } else if (item.result == "EXIST") {
						alert("您已参加过本次调研了！");
						window.location='index.php';
                        return false;
                    } else if (item.result == "LIMIT") {
						alert("感谢您的积极参与，本次活动已结束，如您有任何建议或意见，欢迎发送邮件至research@chongbang.com ");
                        window.location='index.php';
                        return false;
                    } else if (item.result == "OK") {
                        window.location='sixlist.php';
                        return false;
                    } else {
						window.location='fivelist.php';
                        return false;
					}
            }
        );

	});
});
</script>
</body>
</html>
