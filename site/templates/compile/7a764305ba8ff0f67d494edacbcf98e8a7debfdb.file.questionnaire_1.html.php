<?php /* Smarty version Smarty-3.0.4, created on 2013-12-24 12:13:05
         compiled from "/usr/local/www/Daning_new/wenjuan/site/templates/test99/index/questionnaire_1.html" */ ?>
<?php /*%%SmartyHeaderCode:22425445952b909d1574dc6-85210548%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7a764305ba8ff0f67d494edacbcf98e8a7debfdb' => 
    array (
      0 => '/usr/local/www/Daning_new/wenjuan/site/templates/test99/index/questionnaire_1.html',
      1 => 1387858360,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22425445952b909d1574dc6-85210548',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>大宁国际商业广场调研问卷</title>
<link href="css/css.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
<script>DD_belatedPNG.fix('*'); </script>
<![endif]-->
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/checkFrom.js" type="text/javascript"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-8753756-28']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div id="main">
<div class="top"><a href="http://www.daningdaning.com/cn/" target="_blank" title="大宁国际"></a></div>
<div class="hd">
<div class="tt2"></div>
<div class="hd1"><img src="css/img/m1_1.gif" /><img src="css/img/m2.gif" /><img src="css/img/m3.gif" /><img src="css/img/m4.gif" /></div>
</div>
<div class="center_1">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="center_2">
  <tr>
    <td valign="top" class="center_3">
    <div class="center_5">
      <table width="855" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="word1"><span class="word5">A1</span> 请问您的年龄是：</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="100"><input type="radio" name="a_age" value="18岁以下" />18岁以下</td>
              <td width="100"><input type="radio" name="a_age" value="18-24岁" />18－24岁</td>
              <td width="100"><input type="radio" name="a_age" value="25-29岁" />25－29岁</td>
              <td width="100"><input type="radio" name="a_age" value="30-34岁" />30－34岁</td>
              <td width="100"><input type="radio" name="a_age" value="35-39岁" />35－39岁</td>
              <td width="100"><input type="radio" name="a_age" value="40-50岁" />40－50岁</td>
              <td width="222"><input type="radio" name="a_age" value="50岁以上" />50岁以上</td>
            </tr>
          </table></td>
          </tr>
        <tr>
          <td class="word1"><span class="word5">A2</span> 请问您的家庭结构是：</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="66"><input type="radio" name="a_home_structure" id="one" value="单身" /> 单身</td>
              <td width="92"><input type="radio" name="a_home_structure" id="two" value="两口之家" /> 两口之家</td>
              <td width="156"><input type="radio" name="a_home_structure" id="twomore" value="两代家庭（与父母）" /> 两代家庭（与父母）</td>
              <td width="156"><input type="radio" name="a_home_structure" id="three" value="三口之家（与孩子）" /> 三口之家（与孩子）</td>
              <td><input type="radio" name="a_home_structure" id="threemore" value="三代家庭" /> 三代家庭</td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="30" colspan="5">（如果是后两项，请问您的孩子年龄：       <input type="radio" name="child_age" value="0-5岁" class="children" /> 0－5岁　<input type="radio" name="child_age" value="6-10岁" class="children" /> 6－10岁　<input type="radio" name="child_age" value="11-16岁" class="children" /> 11－16岁　<input type="radio" name="child_age" value="16岁以上" class="children" /> 16岁以上）</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">A3</span> 请问您的家庭月收入？</td>
        </tr>
        <tr>
          <td  class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="118"><input type="radio" name="a_month_income" value="5000元以下" /> 5000元以下</td>
              <td width="138"><input type="radio" name="a_month_income" value="5000-10000元" /> 5000－10000元</td>
              <td width="146"><input type="radio" name="a_month_income" value="10000-20000元" /> 10000－20000元</td>
              <td width="152"><input type="radio" name="a_month_income" value="20000-30000元" /> 20000－30000元</td>
              <td><input type="radio" name="a_month_income" value="20000元以上" /> 30000元以上</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">A4</span> 请问您的职业是？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33" height="28">&nbsp;</td>
              <td width="106" height="28"><input type="radio" name="a_job" value="学生" /> 学生</td>
              <td width="136" height="28"><input type="radio" name="a_job" value="普通公司职员" /> 普通公司职员</td>
              <td width="120" height="28"><input type="radio" name="a_job" value="中高级白领" /> 中高级白领</td>
              <td width="120" height="28"><input type="radio" name="a_job" value="公司管理层" /> 公司管理层</td>
              <td width="120" height="28"><input type="radio" name="a_job" value="国家公务员" /> 国家公务员</td>
              <td height="28"><input type="radio" name="a_job" value="私营业主" /> 私营业主</td>
              </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28"><input type="radio" name="a_job" value="离退休者" /> 离退休者</td>
              <td height="28"><input type="radio" name="a_job" value="家庭主妇" /> 家庭主妇</td>
              <td height="28" colspan="4"><input type="radio" name="a_job" value="自由职业者" /> 自由职业者</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">A5</span> 请问您的最高学历是？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="106"><input type="radio" name="a_edu" value="高中及以下" /> 高中及以下</td>
              <td width="68"><input type="radio" name="a_edu" value="大专" /> 大专</td>
              <td width="68"><input type="radio" name="a_edu" value="本科" /> 本科</td>
              <td><input type="radio" name="a_edu" value="硕士及以上" /> 硕士及以上</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">A6</span> 请问您经常网购么？</td>
        </tr> 
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="100"><input type="radio" value="有" name="a_6" class="myhave a_join_what_select" id="a6_y" /> 是</td>
              <td><input type="radio" value="没有" name="a_6" class="mynone a_join_what_select" id="a6_n" /> 不是（跳过本题）</td>
            </tr>                                  
          </table>
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
               <td width="33" height="30">&nbsp;</td>
              <td colspan="6">如果是，经常购买的是以下哪类产品？（可多选）</td>
            </tr>
            <tr>
              <td width="33" height="30">&nbsp;</td>
              <td width="70"><input type="checkbox" name="a_6_1" class="a_6_1" value="服装" data="1" /> 服装</td>
              <td width="70"><input type="checkbox" name="a_6_1" class="a_6_1" value="鞋子" data="2" /> 鞋子</td>
              <td width="115"><input type="checkbox" name="a_6_1" class="a_6_1" value="包和配饰类" data="3" /> 包和配饰类</td>
              <td width="145"><input type="checkbox" name="a_6_1" class="a_6_1" value="饮料酒水食品类" data="4" /> 饮料酒水食品类</td>
              <td width="100"><input type="checkbox" name="a_6_1" class="a_6_1" value="电子数码" data="5" /> 电子数码</td>
              <td><input type="checkbox" name="a_6_1" class="a_6_1" value="生活百货" data="6" /> 生活百货</td>
            </tr>
            <tr>
              <td height="28">&nbsp;</td>
              <td height="28" colspan="6"> 其他 <input type="text" name="a_6_2" class="iput6 a_6_1" id="a_6_2"  size="20"/></td>
            </tr>                                                   
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">A7</span> 请问您经常使用APP类智能手机应用程序么？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33">&nbsp;</td>
              <td width="106"><input type="radio" name="a_app" value="是" /> 是</td>
              <td width="106"><input type="radio" name="a_app" value="偶尔" /> 偶尔</td>
              <td><input type="radio" name="a_app" value="一般不用" /> 一般不用</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td class="word1"><span class="word5">A8</span> 请问您会经常从网上了解购物信息，或下载优惠劵么？</td>
        </tr>
        <tr>
          <td class="word4 pa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="33"> </td>
              <td width="340"><input type="radio" name="a_goods_info" value="经常查阅商家信息并下载优惠劵" /> 经常查阅商家信息并下载优惠劵</td>
              <td><input type="radio" name="a_goods_info" value="主要查查打折信息什么的，不太用优惠券" /> 主要查查打折信息什么的，不太用优惠券</td>
              </tr>
            <tr>
              <td> </td>
              <td><input type="radio" name="a_goods_info" value="偶尔会上上点评网之类的，查一下商家信息" /> 偶尔会上上点评网之类的，查一下商家信息</td>
              <td><input type="radio" name="a_goods_info" value="基本不在网上了解这些" /> 基本不在网上了解这些</td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td><a class="a1" id="nextTwo" href="javascript:void(0);">下一步</a></td>
        </tr>
      </table>
    </div>
    </td>
  </tr>
  <tr>
    <td height="38" class="center_4">&nbsp;</td>
  </tr>
</table>
</div>
</div>
<script language="JavaScript">
$(document).ready(function(){
	$(".children").attr( "disabled", "disabled");
	$('#a_6_2').attr("disabled","disabled");	
	$("#one,#two,#twomore").click(function(){
		if(this.checked==true){
			$(".children").attr( "disabled", "disabled");
			assitant_check("child_age", false);
		}
	});
	$("#three,#threemore").click(function(){
		if(this.checked==true){
			$(".children").attr( "disabled", "");
		}
	});
    $("#a6_n").click(function(){
        $('.a_6_1').each(function(){
            $(this).attr( "disabled","disabled");
			$(this).attr('checked', false);
        });
		$("#a_6_2").val('');
    })
    $("#a6_y").click(function(){
        $('.a_6_1').each(function(){
            $(this).attr( "disabled","");
        });     
    })
	
	$("#nextTwo").click(function(){
		var a_age = $('input[name=a_age]:checked').val();
		var a_home_structure = $('input[name=a_home_structure]:checked').val();
		var a_month_income = $('input[name=a_month_income]:checked').val();
		var a_job = $('input[name=a_job]:checked').val();
		var a_edu = $('input[name=a_edu]:checked').val();
		var a_6 = $('input[name=a_6]:checked').val();
		var a_6_1 = '';
		var a_app = $('input[name=a_app]:checked').val();
		var a_goods_info = $('input[name=a_goods_info]:checked').val();
		var child_age = '';
        if ($('input[name=child_age]:checked').val() == undefined || $('input[name=child_age]:checked').val() == '') {
			child_age = '';
		} else {
			child_age = $('input[name=child_age]:checked').val();
		}

        if(a_age == undefined || a_age == ''){
            alert('请回答A1');
            
            return false;
        }

        if(a_home_structure == undefined || a_home_structure == ''){
            alert('请回答A2');
            return false;
        }

        if(a_month_income == undefined || a_month_income == ''){
            alert('请回答A3');
            return false;
        }

        if(a_job == undefined || a_job == ''){
            alert('请回答A4');
            return false;
        }

        if(a_edu == undefined || a_edu == ''){
            alert("请回答A5"); 
            return false;
        }
		
		if(a_6 == undefined || a_6 == ''){
			alert("请回答A6"); 
            return false;
		}else{
			if(a_6 == '有'){
				a_6_1 = checkbox('a_6_1');
				var a_6_2 = $('input[name=a_6_2]').val();
				if(a_6_2){
					a_6_1 += a_6_2+'；';
				}
				if(!a_6_1){
					alert('请回答A6');
					return false;
				}else{
					a_6 += ':'+a_6_1;
				}
			}
		}
		
		
        if(a_app == undefined || a_app == ''){
            alert("请回答A7"); 
            return false;
        }

        if(a_goods_info == undefined || a_goods_info == ''){
            alert("请回答A8"); 
            return false;
        }		

		if (child_age != '') {
			a_home_structure = a_home_structure + '，小孩年龄：' + child_age;
		}
		
		
		function checkbox(checkbox_name)
		{
			var str=document.getElementsByName(checkbox_name);
			var chestr = '';
			var objarray=str.length;
			for (i=0;i<objarray;i++)
			{
			  if(str[i].checked == true)
			  {
			   chestr+=str[i].value+"；";
			  }
			}
			return chestr;		
		}		

		$.post("stepTwo.php?t="+Math.random(),
            {
                a_age:a_age,
                a_home_structure:a_home_structure,		
                a_month_income:a_month_income,
                a_job:a_job,
                a_edu:a_edu,
				a_6:a_6,
				a_app:a_app,
				a_goods_info:a_goods_info
            },
            function(data){
                var item = eval("("+data+")");
					//alert(item);
                    if (item.result == "INPUT_INVALID") {
                        alert("表单填写不完整！");
                    } else if (item.result == "NONE") {
						alert("会员卡号填写错误！");
						window.location='index.php';
                    } else if (item.result == "EXIST") {
						alert("您已参与过此次调研，感谢配合！");
						window.location='index.php';
                    } else if (item.result == "LIMIT") {
						alert("调研已结束，感谢您的积极参与！");
                        window.location='index.php';
                    } else if (item.result == "OK") {
                        window.location='threelist.php';
                    } else {
						window.location='twolist.php';
					}
            }
        );

	});
});
</script>
</body>
</html>

