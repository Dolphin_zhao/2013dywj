<?php
import('imag.utilities.pagination');
import('imag.utilities.pagestyle');
import('operator.navi.admincontroller');

class UserController extends AdminController
{
    private $echo_type = 'json';
	private $daningdb  = null;

    function __construct($config = array())
    {
        parent::__construct($config);
		/*
        $options = array(
            'host' 	   => 'localhost',
            'user' 	   => 'gary',
            'password' => 'say1171i',
            'database' => 'daning_new',
            'driver'   => 'mysql'
        );
        $this->daningdb = & Database::getInstance($options);
		*/
		include("models/mysql.php");
        $host     = 'localhost';
        $user     = 'gary';
        $password = 'say1171i';
        $db       = 'daning_new';
        $arr 	  = array("host"=>$host, "user"=>$user, "password"=>$password, "database"=>$db);
        $this->daningdb = new MysqlLink($arr);

		if (!session_id()) {
			session_start();
		}

        $this->registerTask( 'index', 'indexList');
        $this->registerTask( 'twolist', 'twoList');
        $this->registerTask( 'threelist', 'threeList');
        $this->registerTask( 'fourlist', 'fourList');
        $this->registerTask( 'fivelist', 'fiveList');
        $this->registerTask( 'sixlist', 'sixList');
        $this->registerTask( 'stepOne', 'stepOne');
        $this->registerTask( 'stepTwo', 'stepTwo');
        $this->registerTask( 'stepThree', 'stepThree');
        $this->registerTask( 'stepFour', 'stepFour');
        $this->registerTask( 'stepFive', 'stepFive');
        $this->registerTask( 'stepSix', 'stepSix');
        $this->registerTask( 'check_vipcode', 'checkVipcode');
    }

    function display(){
        echo 'display';
    }

    function indexList()
    {
        $view   = $this->createView('index/questionnaire.html');
        $view->assign();
        $view->display();
    }

    function twoList()
    {
        $view   = $this->createView('index/questionnaire_1.html');
        $view->assign();
        $view->display();
    }

	function threeList()
	{
        $view   = $this->createView('index/questionnaire_2.html');
        $view->assign();
        $view->display();
	}

	function fourList()
	{
        $view   = $this->createView('index/questionnaire_3.html');
        $view->assign();
        $view->display();
	}

	function fiveList()
	{
        $view   = $this->createView('index/questionnaire_4.html');
        $view->assign();
        $view->display();
	}

	function sixList()
	{
        $view   = $this->createView('index/questionnaire_5.html');
        $view->assign();
        $view->display();
	}

	function stepOne()
	{
        $vipcode   = Request::getVar('vipcode', 'post');
        $name      = Request::getVar('name', 'post');
        $sex 	   = Request::getVar('sex', 'post');
        $mobile    = Request::getVar('mobile', 'post');
        $area 	   = Request::getVar('area', 'post');
        $street    = Request::getVar('street', 'post');
        $alley 	   = Request::getVar('alley', 'post');
        $number    = Request::getVar('number', 'post');
        $roomnum   = Request::getVar('roomnum', 'post');
        $home_area = Request::getVar('home_area', 'post');

		if ($vipcode == '' || $name == '' || $sex == '' || $mobile == '' || $area == '' || $street == '' || $home_area == '') {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($vipcode);

		$address = '';
		if ($area != '') {
			$address .= $area . '区';
		}
		if ($street != '') {
			$address .= $street . '路(街道)';
		}
		if ($alley != '') {
			$address .= $alley . '弄';
		}
		if ($number != '') {
			$address .= $number . '号';
		}
		if ($roomnum != '') {
			$address .= $roomnum . '室';
		}
		$newOneArr = array(
			'vipcode'   => $vipcode,
			'name'      => $name,
			'sex' 	    => $sex,
			'mobile'    => $mobile,
			'address'   => $address,
			'home_area' => $home_area
		);

		$model  = $this->createModel('user',dirname( __FILE__ ));
		$object = new stdClass();
		foreach ($newOneArr as $key => $value) {
			$object->$key = $value;
		}

		$_SESSION['vipcode'] = $vipcode;
		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				//$this->redirect($forward,'添加成功');
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}

		//$_SESSION['user_info'] = $newOneArr;
		//Response::writeByType('OK', $this->echo_type);
		//exit;
	}

	function stepTwo()
	{
        $a_age   		  = Request::getVar('a_age', 'post');
        $a_home_structure = Request::getVar('a_home_structure', 'post');
        $a_month_income   = Request::getVar('a_month_income', 'post');
        $a_job    		  = Request::getVar('a_job', 'post');
        $a_edu 	   		  = Request::getVar('a_edu', 'post');

		if ($a_age == '' || $a_home_structure == '' || $a_month_income == '' || $a_job == '' || $a_edu == '') {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($_SESSION['vipcode']);

		$stTwoArr				      = array();
		$stTwoArr['a_age'] 		      = $a_age;
		$stTwoArr['a_home_structure'] = $a_home_structure;
		$stTwoArr['a_month_income']   = $a_month_income;
		$stTwoArr['a_job']   		  = $a_job;
		$stTwoArr['a_edu']   		  = $a_edu;
		$stTwoArr['vipcode']		  = $_SESSION['vipcode'];

		$model  = $this->createModel('user',dirname( __FILE__ ));
		$object = new stdClass();
		foreach ($stTwoArr as $key => $value) {
			$object->$key = $value;
		}

		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}

	}

	function stepThree()
	{
        $b_shop_times = Request::getVar('b_shop_times', 'post');
        $b_shop_day   = Request::getVar('b_shop_day', 'post');
        $b_shop_with  = Request::getVar('b_shop_with', 'post');
        $b_shop_pay   = Request::getVar('b_shop_pay', 'post');

		if ($b_shop_times == '' || $b_shop_day == '' || $b_shop_with == '' || $b_shop_pay == '') {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($_SESSION['vipcode']);

		$stThreeArr				    = array();
		$stThreeArr['b_shop_times'] = $b_shop_times;
		$stThreeArr['b_shop_day']   = $b_shop_day;
		$stThreeArr['b_shop_with']  = $b_shop_with;
		$stThreeArr['b_shop_pay']   = $b_shop_pay;
		$stThreeArr['vipcode']	    = $_SESSION['vipcode'];

		$model  = $this->createModel('user',dirname( __FILE__ ));

		$object = new stdClass();
		foreach ($stThreeArr as $key => $value) {
			$object->$key = $value;
		}

		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}
	}

	function stepFour()
	{
        $c_go_time		   = Request::getVar('c_go_time', 'post');
        $c_transport 	   = Request::getVar('c_transport', 'post');
        $c_spend_roadtime  = Request::getVar('c_spend_roadtime', 'post');
        $c_purpose 		   = Request::getVar('c_purpose', 'post');
        $c_time_in 		   = Request::getVar('c_time_in', 'post');
        $c_spend_cause 	   = Request::getVar('c_spend_cause', 'post');
        $c_discount 	   = Request::getVar('c_discount', 'post');
        $c_effect 		   = Request::getVar('c_effect', 'post');
        $c_pay_month 	   = Request::getVar('c_pay_month', 'post');
        $c_know_us 		   = Request::getVar('c_know_us', 'post');
        $c_join_what 	   = Request::getVar('c_join_what', 'post');
        $c_vipcode_paytime = Request::getVar('c_vipcode_paytime', 'post');
        $c_interest 	   = Request::getVar('c_interest', 'post');
        $c_idea_vipcode    = Request::getVar('c_idea_vipcode', 'post');

		if ($c_go_time == '' || $c_transport == '' || $c_spend_roadtime == '' || $c_purpose == '' || $c_time_in == '' || $c_spend_cause == '' || $c_discount == '' || $c_effect == '' || $c_pay_month == '' || $c_know_us == '' || $c_join_what == '' || $c_vipcode_paytime == '' || $c_interest == '' || $c_idea_vipcode == '') {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($_SESSION['vipcode']);

		$stFourArr				    = array();
		$stFourArr['c_go_time'] 		= $c_go_time;
		$stFourArr['c_transport'] 		= $c_transport;
		$stFourArr['c_spend_roadtime'] 	= $c_spend_roadtime;
		$stFourArr['c_purpose'] 		= $c_purpose;
		$stFourArr['c_time_in']			= $c_time_in;
		$stFourArr['c_spend_cause']		= $c_spend_cause;
		$stFourArr['c_discount'] 		= $c_discount;
		$stFourArr['c_effect'] 			= $c_effect;
		$stFourArr['c_pay_month']		= $c_pay_month;
		$stFourArr['c_know_us'] 		= $c_know_us;
		$stFourArr['c_join_what'] 	    = $c_join_what;
		$stFourArr['c_vipcode_paytime'] = $c_vipcode_paytime;
		$stFourArr['c_interest']        = $c_interest;
		$stFourArr['c_idea_vipcode']	= $c_idea_vipcode;
		$stFourArr['vipcode']	    	= $_SESSION['vipcode'];

		$model  = $this->createModel('user',dirname( __FILE__ ));

		$object = new stdClass();
		foreach ($stFourArr as $key => $value) {
			$object->$key = $value;
		}

		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}

	}

	function stepFive()
	{
        $d_spend_clothes	= Request::getVar('d_spend_clothes', 'post');
        $d_spend_house 	    = Request::getVar('d_spend_house', 'post');
        $d_spend_electron   = Request::getVar('d_spend_electron', 'post');
        $d_spend_clock 		= Request::getVar('d_spend_clock', 'post');
        $d_spend_recreation = Request::getVar('d_spend_recreation', 'post');
        $d_spend_food 	    = Request::getVar('d_spend_food', 'post');
        $d_spend_face 	    = Request::getVar('d_spend_face', 'post');
        $d_spend_fitness 	= Request::getVar('d_spend_fitness', 'post');
        $d_spend_video 		= Request::getVar('d_spend_video', 'post');
        $d_spend_market 	= Request::getVar('d_spend_market', 'post');
        $d_spend_f8 		= Request::getVar('d_spend_f8', 'post');
        $d_spend_bread		= Request::getVar('d_spend_bread', 'post');
        $d_spend_edu 		= Request::getVar('d_spend_edu', 'post');
        $d_spend_child		= Request::getVar('d_spend_child', 'post');
        $d_food_pay 		= Request::getVar('d_food_pay', 'post');
        $d_food_timein 		= Request::getVar('d_food_timein', 'post');
        $d_merchant_type 	= Request::getVar('d_merchant_type', 'post');
        $d_clothes_spend	= Request::getVar('d_clothes_spend', 'post');
        $d_clothes_type		= Request::getVar('d_clothes_type', 'post');
        $d_play				= Request::getVar('d_play', 'post');

		if ($d_spend_clothes == '' ||
			$d_spend_house == '' ||
			$d_spend_electron == '' ||
			$d_spend_clock == '' ||
			$d_spend_recreation == '' ||
			$d_spend_food == '' ||
			$d_spend_face == '' ||
			$d_spend_fitness == '' ||
			$d_spend_video == '' ||
			$d_spend_market == '' ||
			$d_spend_f8 == '' ||
			$d_spend_bread == '' ||
			$d_spend_edu == '' ||
			$d_spend_child == '' ||
			$d_food_pay == '' ||
			$d_food_timein == '' ||
			$d_merchant_type == '' ||
			$d_clothes_spend == '' ||
			$d_clothes_type == '' ||
			$d_play == '') {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($_SESSION['vipcode']);

		$d_spend = $d_spend_clothes . ';' .
				$d_spend_house . ';' .
				$d_spend_electron . ';' .
				$d_spend_clock . ';' .
				$d_spend_recreation . ';' .
				$d_spend_food . ';' .
				$d_spend_face . ';' .
				$d_spend_fitness . ';' .
				$d_spend_video . ';' .
				$d_spend_market . ';' .
				$d_spend_f8 . ';' .
				$d_spend_bread . ';' .
				$d_spend_edu . ';' .
				$d_spend_child;

		$stFiveArr					  = array();
		$stFiveArr['d_spend'] 		  = $d_spend;
		$stFiveArr['d_food_pay'] 	  = $d_food_pay;
		$stFiveArr['d_food_timein']   = $d_food_timein;
		$stFiveArr['d_merchant_type'] = $d_merchant_type;
		$stFiveArr['d_clothes_spend'] = $d_clothes_spend;
		$stFiveArr['d_clothes_type']  = $d_clothes_type;
		$stFiveArr['d_play'] 		  = $d_play;
		$stFiveArr['vipcode']		  = $_SESSION['vipcode'];

		$model  = $this->createModel('user',dirname( __FILE__ ));

		$object = new stdClass();
		foreach ($stFiveArr as $key => $value) {
			$object->$key = $value;
		}

		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}

	}


	function stepSix()
	{
        $e_suggest     = Request::getVar('e_suggest', 'post');
        $e_add_food    = Request::getVar('e_add_food', 'post');
        $e_add_clothes = Request::getVar('e_add_clothes', 'post');
        $e_add_other   = Request::getVar('e_add_other', 'post');
        $e_add_relax   = Request::getVar('e_add_relax', 'post');
        $e_opinion	   = Request::getVar('e_opinion', 'post');
        $e_tel_order   = Request::getVar('e_tel_order', 'post');

		if ($e_suggest == '' || $e_add_food == '' || $e_add_clothes == '' || $e_add_other == '' || $e_add_relax == '' || $e_tel_order == '') {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($_SESSION['vipcode']);

		$stSixArr				 = array();
		$stSixArr['e_suggest']   = $e_suggest;
		$stSixArr['e_add_extra'] = $e_add_food . ';' . $e_add_clothes . ';' . $e_add_other . ';' . $e_add_relax;
		$stSixArr['e_opinion']   = $e_opinion;
		$stSixArr['e_tel_order'] = $e_tel_order;
		$stSixArr['status'] 	 = 'over';
		$stSixArr['ip']   		 = $this->getIP();
		$stSixArr['vipcode']	 = $_SESSION['vipcode'];

		$model  = $this->createModel('user',dirname( __FILE__ ));

		$object = new stdClass();
		foreach ($stSixArr as $key => $value) {
			$object->$key = $value;
		}

		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				//$this->redirect($forward,'添加成功');
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}
	}

	function checkVipcode()
	{
		//$model = $this->createModel('users', dirname( __FILE__ ),array('dbo'=>$this->daningdb));
		$vipcode = Request::getVar('vipcode', 'post');
		$this->checkUser($vipcode);
		Response::writeByType('FIRST', $this->echo_type);
		exit;
	}

	function checkUser(&$vipcode)
	{
		if (!isset($vipcode)) {
			Response::writeByType('COOKIELIMIT', $this->echo_type);
			exit;
		}

		$model  = $this->createModel('user',dirname( __FILE__ ));
		$uTotal = $model->getTotal();
		if ($uTotal >= 330) {
			Response::writeByType('LIMIT', $this->echo_type);
			exit;
		}

		$this->daningdb->query(" set names utf8 ");
		$sql    = " SELECT count(*) as total FROM `vip_info` WHERE `xf_vipcode` = '{$vipcode}' ORDER BY `xf_jointdate` DESC LIMIT 1";
        $result = $this->daningdb->query($sql);
        $row    = $this->daningdb->fetchrow($result);
		if ($row['total'] < 1) {
			Response::writeByType('NONE', $this->echo_type);
			exit;
		}
		@$this->daningdb->freeresult($result);
		@$this->daningdb->close();

		$model  = $this->createModel('user',dirname( __FILE__ ));
		$user = $model->checkUserExist($vipcode);
		if ($user['status'] == 'over') {
			Response::writeByType('EXIST', $this->echo_type);
			exit;
		}
	}

	function getIP() {
		if (@$_SERVER['HTTP_X_FORWARDED_FOR'])
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if (@$_SERVER['HTTP_CLIENT_IP'])
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		else if (@$_SERVER['REMOTE_ADDR'])
			$ip = $_SERVER['REMOTE_ADDR'];
		else if (@getenv('HTTP_X_FORWARDED_FOR'))
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		else if (@getenv('HTTP_CLIENT_IP'))
			$ip = getenv('HTTP_CLIENT_IP');
		else if (@getenv('REMOTE_ADDR'))
			$ip = getenv('REMOTE_ADDR');
		else
			$ip = 'Unknown';
		return $ip;
	}

}

