<?php
import('imag.utilities.pagination');
import('imag.utilities.pagestyle');
import('operator.navi.admincontroller');

class UserController extends AdminController
{
    private $echo_type = 'json';
	private $daningdb  = null;

    function __construct($config = array())
    {
        parent::__construct($config);
		/*
        $options = array(
            'host' 	   => 'localhost',
            'user' 	   => 'gary',
            'password' => 'say1171i',
            'database' => 'daning_new',
            'driver'   => 'mysql'
        );
        $this->daningdb = & Database::getInstance($options);
		*/
		include("models/mysql.php");
        $host     = 'localhost';
        $user     = 'gary';
        $password = 'say1171i';
        $db       = 'daning_new';
        $arr 	  = array("host"=>$host, "user"=>$user, "password"=>$password, "database"=>$db);
        $this->daningdb = new MysqlLink($arr);

		if (!session_id()) {
			session_start();
		}

        $this->registerTask( 'index', 'indexList');
        $this->registerTask( 'twolist', 'twoList');
        $this->registerTask( 'threelist', 'threeList');
        $this->registerTask( 'fourlist', 'fourList');
        $this->registerTask( 'fivelist', 'fiveList');
        $this->registerTask( 'sixlist', 'sixList');
        $this->registerTask( 'stepOne', 'stepOne');
        $this->registerTask( 'stepTwo', 'stepTwo');
        $this->registerTask( 'stepThree', 'stepThree');
        $this->registerTask( 'stepFour', 'stepFour');
        $this->registerTask( 'stepFive', 'stepFive');
        $this->registerTask( 'stepSix', 'stepSix');
        $this->registerTask( 'check_vipcode', 'checkVipcode');
    }

    function display(){
        echo 'display';
    }

    function indexList()
    {
        $view   = $this->createView('index/index.html');
        $view->assign();
        $view->display();
    }

    function twoList()
    {
        $view   = $this->createView('index/questionnaire_1.html');
        $view->assign();
        $view->display();
    }

	function threeList()
	{
        $view   = $this->createView('index/questionnaire_2.html');
        $view->assign();
        $view->display();
	}

	function fourList()
	{
        $view   = $this->createView('index/questionnaire_3.html');
        $view->assign();
        $view->display();
	}

	function fiveList()
	{
        $view   = $this->createView('index/questionnaire_4.html');
        $view->assign();
        $view->display();
	}

	function sixList()
	{
        $view   = $this->createView('index/questionnaire_5.html');
        $view->assign();
        $view->display();
	}

	function stepOne()
	{
        $vipcode   = Request::getVar('vipcode', 'post');
        $name      = Request::getVar('name', 'post');
        $sex 	   = Request::getVar('sex', 'post');
        $mobile    = Request::getVar('mobile', 'post');
        $area 	   = Request::getVar('area', 'post');
        $street    = Request::getVar('street', 'post');
        $alley 	   = Request::getVar('alley', 'post');
        $number    = Request::getVar('number', 'post');
        $roomnum   = Request::getVar('roomnum', 'post');
        $home_area = Request::getVar('home_area', 'post');

		if ($vipcode == '' || $name == '' || $sex == '' || $mobile == '' || $area == '' || $street == '' || $home_area == '') {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($vipcode);

		$address = '';
		if ($area != '') {
			$address .= $area . '区';
		}
		if ($street != '') {
			$address .= $street . '路(街道)';
		}
		if ($alley != '') {
			$address .= $alley . '弄';
		}
		if ($number != '') {
			$address .= $number . '号';
		}
		if ($roomnum != '') {
			$address .= $roomnum . '室';
		}
		$newOneArr = array(
			'vipcode'   => $vipcode,
			'name'      => $name,
			'sex' 	    => $sex,
			'mobile'    => $mobile,
			'address'   => $address,
			'home_area' => $home_area
		);

		$model  = $this->createModel('user',dirname( __FILE__ ));
		$object = new stdClass();
		foreach ($newOneArr as $key => $value) {
			$object->$key = $value;
		}

		$_SESSION['vipcode'] = $vipcode;
		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				//$this->redirect($forward,'添加成功');
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}

		//$_SESSION['user_info'] = $newOneArr;
		//Response::writeByType('OK', $this->echo_type);
		//exit;
	}

	function stepTwo()
	{
        $a_age   		  = Request::getVar('a_age', 'post');
        $a_home_structure = Request::getVar('a_home_structure', 'post');
        $a_month_income   = Request::getVar('a_month_income', 'post');
        $a_job    		  = Request::getVar('a_job', 'post');
        $a_edu 	   		  = Request::getVar('a_edu', 'post');
		$a_olshop 	   		  = Request::getVar('a_6', 'post');
		$a_app 	   		  = Request::getVar('a_app', 'post');
		$a_goods_info 	   		  = Request::getVar('a_goods_info', 'post');


		if ($a_age == '' || $a_home_structure == '' || $a_month_income == '' || $a_job == '' || $a_edu == '' || $a_olshop == '' || $a_app == '' || $a_goods_info == '') {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($_SESSION['vipcode']);

		$a_olshop = rtrim($a_olshop, "；");
		
		$stTwoArr				      = array();
		$stTwoArr['a_age'] 		      = $a_age;
		$stTwoArr['a_home_structure'] = $a_home_structure;
		$stTwoArr['a_month_income']   = $a_month_income;
		$stTwoArr['a_job']   		  = $a_job;
		$stTwoArr['a_edu']   		  = $a_edu;
		$stTwoArr['a_olshop']   		  = $a_olshop;
		$stTwoArr['a_app']   		  = $a_app;
		$stTwoArr['a_goods_info']   		  = $a_goods_info;
		$stTwoArr['vipcode']		  = $_SESSION['vipcode'];

		$model  = $this->createModel('user',dirname( __FILE__ ));
		$object = new stdClass();
		foreach ($stTwoArr as $key => $value) {
			$object->$key = $value;
		}

		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}

	}

	function stepThree()
	{
        $b_1 = Request::getVar('b_1', 'post');
        $b_2 = Request::getVar('b_2', 'post');
        $b_3 = Request::getVar('b_3', 'post');
        $b_4 = Request::getVar('b_4', 'post');
        $b_5 = Request::getVar('b_5', 'post');
        $b_6 = Request::getVar('b_6', 'post');
        $b_7 = Request::getVar('b_7', 'post');
		$b_8 = Request::getVar('b_8', 'post');
		$b_9 = Request::getVar('b_9', 'post');

		if ($b_1 == '' || $b_2 == '' || $b_3 == '' || $b_4 == '' || $b_6 == '' || $b_7 == '' || $b_8 == '' || $b_9 == '') {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($_SESSION['vipcode']);

		$stThreeArr				    = array();
		$stThreeArr['b1']   = $b_1;
		$stThreeArr['b2']   = $b_2;
		$stThreeArr['b3']   = $b_3;
		$stThreeArr['b4']   = $b_4;
        $stThreeArr['b5']   = $b_5;
        $stThreeArr['b6']   = $b_6;
        $stThreeArr['b7']   = $b_7;
		$stThreeArr['b8']   = $b_8;
		$stThreeArr['b9']   = $b_9;		
		$stThreeArr['vipcode']	    = $_SESSION['vipcode'];

		$model  = $this->createModel('user',dirname( __FILE__ ));

		$object = new stdClass();
		foreach ($stThreeArr as $key => $value) {
			$object->$key = $value;
		}

		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}
	}

	function stepFour()
	{
        $c_1 = Request::getVar('c_1', 'post');
        $c_2 = Request::getVar('c_2', 'post');                              
        $c_3 = Request::getVar('c_3', 'post');
        $c_4 = Request::getVar('c_4', 'post');
        $c_5 = Request::getVar('c_5', 'post');
        $c_6 = Request::getVar('c_6', 'post');
        $c_7 = Request::getVar('c_7', 'post');
        $c_8 = Request::getVar('c_8', 'post');
        $c_9 = Request::getVar('c_9', 'post'); 
        
        
        if ($c_1 == '' || $c_2 == '' || $c_3 == '' || $c_4 == '' || $c_5 == '' || $c_6 == '' || $c_7 == '' || $c_8 == '' || $c_9 == '') {
            Response::writeByType('INPUT_INVALID',$this->echo_type);
            exit;
        }

		$this->checkUser($_SESSION['vipcode']);

		$stFourArr				= array();
		$stFourArr['c1'] 		= $c_1;
		$stFourArr['c2'] 		= $c_2;
		$stFourArr['c3'] 	    = $c_3;
		$stFourArr['c4'] 		= $c_4;
		$stFourArr['c5']		= $c_5;
		$stFourArr['c6']		= $c_6;
		$stFourArr['c7'] 		= $c_7;
		$stFourArr['c8'] 		= $c_8;
		$stFourArr['c9']		= $c_9;
		$stFourArr['vipcode']	= $_SESSION['vipcode'];
        

		$model  = $this->createModel('user',dirname( __FILE__ ));

		$object = new stdClass();
		foreach ($stFourArr as $key => $value) {
			$object->$key = $value;
		}

		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}

	}

	function stepFive()
	{

        $d_1 = Request::getVar('d_1', 'post');                             
        $d_2 = Request::getVar('d_2', 'post');
        $d_3 = Request::getVar('d_3', 'post');
        $d_4 = Request::getVar('d_4_1', 'post');
        $d_5 = Request::getVar('d_5', 'post');
        $d_6 = Request::getVar('d_6', 'post'); 
        $d_7 = Request::getVar('d_7_1', 'post');
        $d_8 = Request::getVar('d_8', 'post');                                                 
        

		if ($d_1 == '' ||
			$d_2 == '' ||
			$d_3 == '' ||
			$d_4 == '' ||
			$d_5 == '' ||
			$d_6 == '' ||
            $d_7 == '' ||
            $d_8 == '' ) {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($_SESSION['vipcode']);

                             
		$stFiveArr			    = array();
		$stFiveArr['d1']        = $d_1;
		$stFiveArr['d2'] 	    = $d_2;
		$stFiveArr['d3']        = $d_3;
		$stFiveArr['d4']        = $d_4;
		$stFiveArr['d5']        = $d_5;
		$stFiveArr['d6']        = $d_6;
		$stFiveArr['d7'] 		= $d_7;
        $stFiveArr['d_opinion']        = $d_8;
		$stFiveArr['status'] 	= 'over';
		$stFiveArr['ip']   		= $this->getIP();
                        
		$stFiveArr['vipcode']		  = $_SESSION['vipcode'];

		$model  = $this->createModel('user',dirname( __FILE__ ));

		$object = new stdClass();
		foreach ($stFiveArr as $key => $value) {
			$object->$key = $value;
		}

		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}

	}


	function stepSix()
	{
        $e_1     = Request::getVar('e_1', 'post');
        $e_2     = Request::getVar('e_2', 'post');
        $e_3     = Request::getVar('e_3', 'post');
        $e_4     = Request::getVar('e_4', 'post');

		if ($e_1 == '' || $e_2 == '' || $e_3 == '') {
			Response::writeByType('INPUT_INVALID',$this->echo_type);
			exit;
		}

		$this->checkUser($_SESSION['vipcode']);

		$stSixArr				= array();
		$stSixArr['e1']         = $e_1;
		$stSixArr['e2']         = $e_2;
		$stSixArr['e3']         = $e_3;
		$stSixArr['e4']         = $e_4;
		$stSixArr['status'] 	= 'over';
		$stSixArr['ip']   		= $this->getIP();
		$stSixArr['vipcode']	= $_SESSION['vipcode'];

		$model  = $this->createModel('user',dirname( __FILE__ ));

		$object = new stdClass();
		foreach ($stSixArr as $key => $value) {
			$object->$key = $value;
		}

		$user = $model->checkUserExist($_SESSION['vipcode']);
		if ($user) {
			if ($model->update($object, 'vipcode')) {
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		} else {
			if ($model->store($object)) {
				//$this->redirect($forward,'添加成功');
				Response::writeByType('OK', $this->echo_type);
				exit;
			} else {
				Response::writeByType('FAIL', $this->echo_type);
				exit;
			}
		}
	}

	function checkVipcode()
	{
		//$model = $this->createModel('users', dirname( __FILE__ ),array('dbo'=>$this->daningdb));
		$vipcode = Request::getVar('vipcode', 'post');
		$this->checkUser($vipcode);
		Response::writeByType('FIRST', $this->echo_type);
		exit;
	}

	function checkUser(&$vipcode)
	{
		if (!isset($vipcode)) {
			Response::writeByType('COOKIELIMIT', $this->echo_type);
			exit;
		}

		$model  = $this->createModel('user',dirname( __FILE__ ));
		$uTotal = $model->getTotal();
		if ($uTotal >= 300) {
			Response::writeByType('LIMIT', $this->echo_type);
			exit;
		}

		$this->daningdb->query(" set names utf8 ");
		$sql    = " SELECT count(*) as total FROM `vip_info` WHERE `xf_vipcode` = '{$vipcode}' ORDER BY `xf_jointdate` DESC LIMIT 1";
        $result = $this->daningdb->query($sql);
        $row    = $this->daningdb->fetchrow($result);
		if ($row['total'] < 1) {
			Response::writeByType('NONE', $this->echo_type);
			exit;
		}
		@$this->daningdb->freeresult($result);
		@$this->daningdb->close();

		$model  = $this->createModel('user',dirname( __FILE__ ));
		$user = $model->checkUserExist($vipcode);
		if (!empty($user) && $user['status'] == 'over') {
			Response::writeByType('EXIST', $this->echo_type);
			exit;
		}
	}

	function getIP() {
		if (@$_SERVER['HTTP_X_FORWARDED_FOR'])
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if (@$_SERVER['HTTP_CLIENT_IP'])
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		else if (@$_SERVER['REMOTE_ADDR'])
			$ip = $_SERVER['REMOTE_ADDR'];
		else if (@getenv('HTTP_X_FORWARDED_FOR'))
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		else if (@getenv('HTTP_CLIENT_IP'))
			$ip = getenv('HTTP_CLIENT_IP');
		else if (@getenv('REMOTE_ADDR'))
			$ip = getenv('REMOTE_ADDR');
		else
			$ip = 'Unknown';
		return $ip;
	}

}

