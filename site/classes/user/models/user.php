<?php
class ModelUser extends Model
{
    var $useradmin = null;
    var $table     = '2013dywj';

    function __construct()
    {
        parent::__construct();
        $this->_db->query("SET NAMES 'utf8'");
    }

    function store($data,$table=''){
        $table = "{$this->table}";
        if(!$this->_db->insertObject($table,$data)){
            return false;
        }else{
            return $this->_db->insertid();
        }
    }

    function update($data,$aid,$table=''){
        $table = "{$this->table}";
        return $this->_db->updateObject($table,$data,$aid);
    }

	function checkUserExist($vipcode)
	{
		$list = array();
		$sql = "SELECT * FROM `{$this->table}` WHERE  `vipcode`='{$vipcode}'";

		if( ($result = $this->_db->query($sql)) ){
			if( ($row = $this->_db->fetchrow($result)) ){
				$list = $row;
			}
			$this->_db->freeresult($result);
		}
		return $list;
	}

	function getTotal()
	{
		$list = 0;
		$sql = "SELECT count(*) as total FROM `{$this->table}`  WHERE `status`='over' ";

		if( ($result = $this->_db->query($sql)) ){
			if( ($row = $this->_db->fetchrow($result)) ){
				$list = $row['total'];
			}
			$this->_db->freeresult($result);
		}
		return $list;
	}
}
?>
