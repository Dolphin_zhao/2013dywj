<?php
import('imag.utilities.pagination');
import('imag.utilities.pagestyle');
import('operator.navi.admincontroller');

class UserController extends AdminController
{
	/**
	 * Constructor
	 *
	 * @params	array	Controller configuration array
	 */
	var $privilegeList;
	function __construct($config = array())
	{
		parent::__construct($config);
		
		$privilegeConfig = Config::getConfig("privilege");
		if(!($this->checkPrivilege($privilegeConfig->ADMIN_PRIVILEGE))){
			$this->redirect("/operator/right.php","您没有权限访问该页面！");
		}
        
		$this->registerTask( 'list','userList');
		$this->registerTask( 'create','create');
		$this->registerTask( 'insert','insert');
		$this->registerTask( 'modify','modify');
		$this->registerTask( 'update','update');
		$this->registerTask( 'delete','delete');
        
	}
	function display(){
		echo "display";
	}
	
	function userList()
	{
		$p = Request::getVar('p','get');
        
		$model = $this->createModel("user",dirname( __FILE__ ));
		
		if(empty($p)){$p=1;}
		$base_url = "list.php";
		$per_page = 20;
		
		$style = new PageStyle();
		$start = ($p-1)*$per_page;
		$userList = $model->getUserList($start,$per_page);
		$total_item = $model->getTotal();

		$options = array(
			"baseurl"	 => $base_url,
			"totalitems" => $total_item,
			"perpage"	 => $per_page,
			"page"	     => $p,
			"maxpage"	 => 15,
			"pagestyle"  => $style,
			"showtotal"  => false
		);
		$pagination = new Pagination($options);
		$p = $pagination->getPage();
		for($i=0;$i<count($userList);$i++){
			$p = array();
			$privileges = explode(",",$userList[$i]["privilege"]);
			for($a=0;$a<count($privileges);$a++){
				if(array_key_exists($privileges[$a],$this->privilegeList)){
					$p[] = $this->privilegeList[$privileges[$a]]["title"];
				}
			}
			$userList[$i]["privilege"] = implode(",",$p);
		}
		
		$view  = $this->createView("operator/adminuser/userList.html");
		$object = new stdClass();
		$object->PAGINATION = $pagination->fetch();
		$object->userList = $userList;
		$view->assign($object);
		$view->display();
	}

	function _getPrivilegeList($uid=""){
		if(empty($uid)){
			return $this->privilegeList;
		}
		$model = $this->createModel("user",dirname( __FILE__ ));
		$privilege = explode(",",$model->getPrivilege($uid));
		$privilegelList = $this->privilegeList;

		for($i=0;$i<count($privilege);$i++){
			if(array_key_exists($privilege[$i],$privilegelList)){
				$privilegelList[$privilege[$i]]["p"] = 1;
			}
		}

		return $privilegelList;
	}
	
	function create()
	{
		$view  = $this->createView("operator/adminuser/create_user.html");
		$object = new stdClass();
		$object->privilegeList = $this->_getPrivilegeList();
		$view->assign($object);
		$view->display();
	}
	
	function insert()
	{
		$forward = Request::getVar("forward");
		if(empty($forward))
		{
			$forward = "list.php";
		}
	
		$username = Request::getVar('username','post');
		$password = Request::getVar('password','post');
		$privileges = Request::getVar('privileges','post');
		
		$password = md5($password);
		
		$model = $this->createModel("user",dirname( __FILE__ ));

		$object = new stdClass();
		$object->username = $username;
		$object->password = $password;
		$object->privilege = implode(",",$privileges);
		$object->ip   = $_SERVER['REMOTE_ADDR'];
		$object->time = gmdate('Y-m-d H:i:s');
		
		if ($model->store($object))
		{
			$this->redirect($forward,"添加成功");
		}
		else
		{
			$this->redirect($forward,"添加失败");
		}
	}
	
	function modify()
	{
		$uid = Request::getVar('uid','get');
		
		$model = $this->createModel("user",dirname( __FILE__ ));
		
		$userInfo = $model->getUserInfo($uid);

		$view  = $this->createView("operator/adminuser/modify_user.html");
		$object = new stdClass();
		$object->privilegeList = $this->_getPrivilegeList($uid);
		$object->user = $userInfo;
		$view->assign($object);
		$view->display();
	}
	
	function update()
	{
		$forward = Request::getVar("forward");
		if(empty($forward))
		{
			$forward = "list.php";
		}

		$password = Request::getVar("passwd","post");
		$repeat_password = Request::getVar("repeat_passwd","post");
		$privileges = Request::getVar('privileges','post');
		$aid = Request::getVar('aid','post');

		if($password!=$repeat_password){
			$this->redirect($forward,"两次输入的密码不一致!");
		}
		if(!empty($password)){
			$password = md5($password);
		}
		$object = new stdClass();
		if(!empty($password)){
			$object->password = $password;
		}
		$object->privilege = implode(",",$privileges);
		$object->aid = $aid;

		$model = $this->createModel("user",dirname( __FILE__ ));
		
		if ($model->update($object,'aid'))
		{
			$this->redirect($forward,"修改成功!");
		}
		else
		{
			$this->redirect($forward,"修改失败!");
		}
	}
    
	function delete()
	{
		$forward = Request::getVar("forward");
		if(empty($forward))
		{
			$forward = "list.php";
		}
		$uid = Request::getVar('uid','get');
		if($uid==1){
			$this->redirect($forward,"此用户不能被删除！");
		}

		$model = $this->createModel("user",dirname( __FILE__ ));
		
		if ($model->delete($uid))
		{
			$this->redirect($forward,"删除成功");
		}
		else
		{
			$this->redirect($forward,"删除失败");
		}
	}

}

