<?php
/**
 * Example model
 * 
 * @package		classes
 * @subpackage	operator.global
 * @author gary wang (wangbaogang123@hotmail.com)
 * 
 */
class ModelBasic extends Model
{

	/**
	 * array
	 */
	var $table = null;
	
	function __construct()
	{
		parent::__construct();
		$this->table = Config::getTable("example");
	}

	function getList($start,$per_page,$where="1")
	{
		$list = null;
		

		$sql = " SELECT * FROM `{$this->table}` WHERE {$where} ORDER BY time DESC LIMIT {$start},{$per_page}";
		
		if( ($result = $this->_db->query($sql)) )
		{
			while ( ($row = $this->_db->fetchrow($result)) )
			{
				$list[] = $row;
			}
		}
		
		$this->_db->freeresult($result);
		return $list;
	}
	
	function getTotal($where)
	{
		$total = 0;
        
		$sql = "SELECT COUNT(*) AS total FROM `{$this->table}` WHERE {$where}";
		
		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$total = $row['total'];
			}
		}
		
		$this->_db->freeresult($result);
		return $total;
	}
	
	function getRow($id)
	{
		$list = null;
		
		$sql = "SELECT * FROM `{$this->table}` WHERE `{$this->table->getKeyName()}`='{$id}'";
		
		if(($result = $this->_db->query($sql)))
		{
			if(($row = $this->_db->fetchrow($result)))
			{
				$list = $row;
			}
		}
		
		$this->_db->freeresult($result);
		return $list;
	}
 
	function delete($id)
	{
		return $this->table->delete($id);
	}
}
?>
