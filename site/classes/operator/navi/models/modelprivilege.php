<?php
/**
 * Privilege Modle
 * Created on 2010-6-18
 * @package		classes
 * @subpackage	operator.navi.models
 * @author gary wang (wangbaogang123@hotmail.com)
 */
 
class ModelPrivilege extends Model
{
	/**
	 * Admin table for operator
	 */
	var $useradmin;
	
	/**
	 * 
	 */
	function __construct($config = array())
	{
		import("config.globalconfig");
		$global = Config::getConfig("global");
		$this->useradmin = Config::getTable($global->admintable);
		unset($global);
		parent::__construct($config);
		
	}

	/**
	 * get privilage
	 */
	function getPrivilege($uid){

		$sql = "SELECT privilege FROM `{$this->useradmin->getTableName()}` WHERE `{$this->useradmin->getKeyName()}`='$uid'";

		if (!($result = $this->_db->query($sql))){
				return array();
		}
			
		if(!($row = $this->_db->fetchrow($result))){
			return array();
		}
		
		$this->_db->freeresult($result);
		return $row['privilege'];

	}

}

?>