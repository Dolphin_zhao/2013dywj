<?php
import('imag.component.controller');
import('imag.component.model');
import('imag.component.view');
import('imag.component.template');
import('imag.database.database');
import('operator.navi.privilegehelper');

/**
 * Provide usually function for operator
 * Created on 2010-8-5
 * 
 * @package		classes
 * @subpackage	operator.navi
 * @author gary wang (wangbaogang123@hotmail.com)
 */
 
class AdminController extends Controller
{	
	function __construct($config = array())
	{
		parent::__construct($config);
	}
	
	/**
	 * get db under no Model model
	 */
	function getDb(){
		import("config.dbconfig");
		$db = & Database::getInstance(DbConfig::getOption());
		return $db;
	}
	
	/**
	 * privilege check privilege for catalog
	 */
	function checkPrivilege($id){
		
		$config = Config::getConfig("privilege");
		$this->privilegeList = $config->privilege();
		array_pop($this->privilegeList);
		$phelper = new PrivilegeHelper();
		return $phelper->checkPrivilege($id);
	}
	
	/**
	 * get privilege used common privilege
	 */
	function getPrivilege(){
		$phelper = new PrivilegeHelper();
		return $phelper->getPrivilege();
	}
	
}
?>
