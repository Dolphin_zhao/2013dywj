<?php
import('imag.component.controller');
import('imag.component.model');
import('imag.component.view');
import('imag.component.template');
import('imag.database.database');

/**
 * Login for admin operator
 * 
 * @package		classes
 * @subpackage	operator.navi
 * @author gary wang (wangbaogang123@hotmail.com)
 */

class UserController extends Controller
{
	/**
	 * Constructor
	 *
	 * @params	array	Controller configuration array
	 */
	function __construct($config = array())
	{

		parent::__construct($config);
		$this->registerTask( 'login','login');
		$this->registerTask( 'logout','logout');
		
	}
	
	/**
	 * login
	 */
	function login()
	{
		$username = Request::getVar("username","post");
		$passwd   = Request::getVar("password","post");
		
		$forward = Request::getVar("forward");
		if(empty($forward))
		{
			$forward = Request::getVar("HTTP_REFERER",'server');
		}
		
		if(empty($username) || empty($passwd))
		{
			$this->redirect($forward,"登录表单填写不完整！");
		}

		$uid = Request::getVar("a_uid","cookie");
		if(!empty($uid))
		{
			setcookie("a_uid", '',-3600, Config::$cookiepath, Config::domain(),0);
			setcookie("a_username", '',-3600, Config::$cookiepath, Config::domain(),0);
		}
		
		$model = $this->createModel("user",dirname( __FILE__ ));
		
		if(!$model->isConnected()){
			//$this->redirect($forward,"数据库连接失败！");
		}

		$row = $model->getLogin($username);
		
		if ($row['password'] == md5($passwd))
		{
            setcookie("a_uid", $row["aid"],time()+60*60*24, Config::$cookiepath, Config::domain(),0);
            setcookie("a_username", $username,time()+60*60*24, Config::$cookiepath, Config::domain(),0);
			
			$this->redirect($forward,"登录成功！");
			
		}else{
			$this->redirect($forward,"用户名或密码错误！");
		}
	}

	/**
	 * logout
	 */
	function logout()
	{
		$forward = Request::getVar("forward");
		if(empty($forward))
		{
			$forward = Request::getVar("HTTP_REFERER",'server');
		}

		$uid = Request::getVar("a_uid","cookie");
		if(isset($uid))
		{
			setcookie("a_uid", '',-3600, Config::$cookiepath, Config::domain(),0);
            setcookie("a_username", '',-3600, Config::$cookiepath, Config::domain(),0);
		}
		
		$this->redirect($forward);

	}

}

