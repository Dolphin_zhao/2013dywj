<?php
import('operator.navi.models.modelprivilege');

/**
 * 
 * Privilege helper
 *
 * @package		classes
 * @subpackage	operator.navi
 * @author gary wang (wangbaogang123@hotmail.com)
 * 
 */

class PrivilegeHelper
{
	/**
	 * Check catalog privilege
	 */
	function checkPrivilege($f){
		$model = new ModelPrivilege();
		$uid = Request::getVar("a_uid","cookie");
		$privilege = explode(",",$model->getPrivilege($uid));
		return in_array($f,$privilege);
	}
	
	/**
	 * Get user privileges 
	 * return string
	 */
	function getPrivilege(){
		$uid = Request::getVar("a_uid","cookie");
		$model = new ModelPrivilege();
		if($model->getPrivilege($uid)==null){
			return "-1";
		}
		return $model->getPrivilege($uid);
	}

}

?>