<?php 
//需要授权才能访问本页面
http_auth();

@header("Content-type: text/html;charset=UTF-8");

$connection    = @mysql_connect('localhost', 'root', 'daningmysql2006');
$db            =  mysql_select_db("campaign", $connection);    
mysql_query(" set names utf8 ");

export();

function export()
{
    global $connection;
    require_once 'PHPExcel.php';  
    require_once 'PHPExcel/Writer/Excel2007.php';    // 用于其他低版本xls  
    $objExcel = new PHPExcel();  
    $objWriter = new PHPExcel_Writer_Excel5($objExcel);    // 用于其他版本格式  
    $objExcel->setActiveSheetIndex(0);    
    $objActSheet = $objExcel->getActiveSheet();  
    $objActSheet->setTitle('大宁国际调研问卷'.date("Y-m-d"));

    $objExcel->getActiveSheet()->getColumnDimension('B')->setWidth('20');
    $objExcel->getActiveSheet()->getColumnDimension('C')->setWidth('20');
    $objExcel->getActiveSheet()->getColumnDimension('D')->setWidth('30');           
    $objExcel->getActiveSheet()->getColumnDimension('E')->setWidth('30');
    $objExcel->getActiveSheet()->getStyle('Q2:Q3000')->getAlignment()->setWrapText(true);  
    
    $objActSheet->setCellValue('A1', 'ID');  
    $objActSheet->setCellValue('B1', '大宁会员卡号');           
    $objActSheet->setCellValue('C1', '姓名');        
    $objActSheet->setCellValue('D1', '性别'); 
    $objActSheet->setCellValue('E1', '手机'); 
    $objActSheet->setCellValue('F1', '家庭（单位）地址'); 
    $objActSheet->setCellValue('G1', '所属区域');
    $objActSheet->setCellValue('H1', 'A1年龄'); 
    $objActSheet->setCellValue('I1', 'A2家庭结构'); 
    $objActSheet->setCellValue('J1', 'A3家庭月收入'); 
    $objActSheet->setCellValue('K1', 'A4职业'); 
    $objActSheet->setCellValue('L1', 'A5最高学历'); 
    $objActSheet->setCellValue('M1', 'B1日常消费占总收入比'); 
    $objActSheet->setCellValue('N1', 'B2消费频率'); 
    $objActSheet->setCellValue('O1', 'B3购物与娱乐和谁一起'); 
    $objActSheet->setCellValue('P1', 'B4每月消费额'); 
    $objActSheet->setCellValue('Q1', 'B5是否经常网购'); 
    $objActSheet->setCellValue('R1', 'B6是否经常使用手机应用');
    $objActSheet->setCellValue('S1', 'B7是否经常了解网购信息');
    $objActSheet->setCellValue('T1', 'C1来大宁国际商业广场的频率');
    $objActSheet->setCellValue('U1', 'C2来大宁国际商业广场主要是为了'); 
    $objActSheet->setCellValue('V1', 'C3来大宁国际消费的主要原因');
    $objActSheet->setCellValue('W1', 'C4通过什么样的交通工具来大宁国际');
    $objActSheet->setCellValue('X1', 'C5来大宁国际花费多少交通时间');
    $objActSheet->setCellValue('Y1', 'C6会为打折或特卖信息特意来大宁国际消费么'); 
    $objActSheet->setCellValue('Z1', 'C7打折与促销对您在哪方面的消费影响较大');
    
    $objActSheet->setCellValue('AA1', 'C8每月在大宁国际的消费大概有多少');  
    $objActSheet->setCellValue('AB1', 'C9从哪里得知大宁国际的折扣及活动信息');           
    $objActSheet->setCellValue('AC1', 'C10是否知道或参加过下列任何大宁国际的推广活动');        
    $objActSheet->setCellValue('AD1', 'C11是否经常使用大宁卡'); 
    $objActSheet->setCellValue('AE1', 'C12对哪项会员卡服务最感兴趣');
     
    $objActSheet->setCellValue('AF1', 'C13对大宁卡最主要的意见'); 
    $objActSheet->setCellValue('AG1', 'C14最近一年来，来大宁的次数有什么变化');
    $objActSheet->setCellValue('AH1', 'D1以下餐饮的消费频率'); 
    $objActSheet->setCellValue('AI1', 'D2平均每月在大宁国际用餐几次'); 
    $objActSheet->setCellValue('AJ1', 'D3对大宁国际餐饮方面的印象'); 
    $objActSheet->setCellValue('AK1', 'D4您觉得大宁餐饮还需要那些种类或品牌'); 
    $objActSheet->setCellValue('AL1', 'D5您在大宁国际单次选购服装一般的消费是'); 
    $objActSheet->setCellValue('AM1', 'D6您及您的家庭在大宁购买服装的情况是'); 
    $objActSheet->setCellValue('AN1', 'D7您在大宁购买不同服装的情况');  
    
    $objActSheet->setCellValue('AO1', 'D8您对大宁国际的服装零售主要有什么印象'); 
    $objActSheet->setCellValue('AP1', 'D9您觉得大宁还应该引进哪些服装品牌'); 
    $objActSheet->setCellValue('AQ1', 'E1你对大宁最迫切的建议'); 
    $objActSheet->setCellValue('AR1', 'E2请您在以下关键词中，挑出三个您对大宁国际最直接的印象');
    $objActSheet->setCellValue('AS1', 'E3周边哪些商业设施您也会经常去');
    $objActSheet->setCellValue('AT1', '您对大宁国际还有什么建议');
    $objActSheet->setCellValue('AU1', '提交时间');       
                                      
         
    $sql = "SELECT * FROM  `2012dcwj` WHERE `status`='over' ORDER BY uid";
    $result = mysql_query($sql, $connection);
    $i = 2;
    while ($row = mysql_fetch_array($result))
    {
        $objActSheet->setCellValue('A'.$i, $row['uid']);  
        $objActSheet->setCellValue('B'.$i, $row['vipcode']);           
        $objActSheet->setCellValue('C'.$i, " ".$row['name']);          
        $objActSheet->setCellValue('D'.$i, " ".$row['sex']); 
        $objActSheet->setCellValue('E'.$i, $row['mobile']);
        
        $objActSheet->setCellValue('F'.$i, $row['address']);
        $objActSheet->setCellValue('G'.$i, $row['home_area']);
        $objActSheet->setCellValue('H'.$i, $row['a_age']);
        $objActSheet->setCellValue('I'.$i, $row['a_home_structure']);
        $objActSheet->setCellValue('J'.$i, $row['a_month_income']);
        $objActSheet->setCellValue('K'.$i, $row['a_job']);
        $objActSheet->setCellValue('L'.$i, $row['a_edu']);
        $objActSheet->setCellValue('M'.$i, $row['b1']);
        $objActSheet->setCellValue('N'.$i, $row['b2']);
        $objActSheet->setCellValue('O'.$i, $row['b3']);
        $objActSheet->setCellValue('P'.$i, $row['b4']);
        $objActSheet->setCellValue('Q'.$i, str_replace(',', ',', $row['b5']));
        $objActSheet->setCellValue('R'.$i, $row['b6']);
        $objActSheet->setCellValue('S'.$i, $row['b7']);
        $objActSheet->setCellValue('T'.$i, $row['c1']);
        $objActSheet->setCellValue('U'.$i, str_replace(';', ';', $row['c2']));
        $objActSheet->setCellValue('V'.$i, str_replace(',', ',', $row['c3']));
        $objActSheet->setCellValue('W'.$i, $row['c4']);
        $objActSheet->setCellValue('X'.$i, $row['c5']);
        $objActSheet->setCellValue('Y'.$i, $row['c6']);
        $objActSheet->setCellValue('Z'.$i, $row['c7']);
        $objActSheet->setCellValue('AA'.$i, $row['c8']);
        $objActSheet->setCellValue('AB'.$i, str_replace(',', ',', $row['c9']));
        $objActSheet->setCellValue('AC'.$i, str_replace(',', ',', $row['c10']));
        $objActSheet->setCellValue('AD'.$i, $row['c11']);
        $objActSheet->setCellValue('AE'.$i, str_replace(',', ',', $row['c12']));
        $objActSheet->setCellValue('AF'.$i, str_replace(',', ',', $row['c13']));
        $objActSheet->setCellValue('AG'.$i, $row['c14']);
        $objActSheet->setCellValue('AH'.$i, str_replace(';', ',', $row['d1']));
        $objActSheet->setCellValue('AI'.$i, $row['d2']);
        $objActSheet->setCellValue('AJ'.$i, str_replace(';', ',', $row['d3']));
        $objActSheet->setCellValue('AK'.$i, $row['d4']);
        $objActSheet->setCellValue('AL'.$i, $row['d5']);
        $objActSheet->setCellValue('AM'.$i, $row['d6']);
        $objActSheet->setCellValue('AN'.$i, str_replace(';', ';', $row['d7']));
        $objActSheet->setCellValue('AO'.$i, str_replace(';', ';', $row['d8']));
        $objActSheet->setCellValue('AP'.$i, $row['d9']);
        $objActSheet->setCellValue('AQ'.$i, str_replace(',', ',', $row['e1']));
        $objActSheet->setCellValue('AR'.$i, str_replace(',', ',', $row['e2']));
        $objActSheet->setCellValue('AS'.$i, str_replace(',', ',', $row['e3']));
        $objActSheet->setCellValue('AT'.$i, $row['e4']);
        $objActSheet->setCellValue('AU'.$i, $row['time']);                                                                       
        $i++;         
    }
      
    $outputFileName = "data/daning_".date("Y-m-d").".xls";  
    $objWriter->save($outputFileName); 
    echo("<a href='".$outputFileName."' target='_blank'>点击下载大宁调研问卷数据</a>");
}

function http_auth()
{
    //需要http认证才能访问本页面
    $username = 'kimi';
    $passwd   = '1111';

    if (!isset($_SERVER['PHP_AUTH_USER'])) 
    {
        header('WWW-Authenticate: Basic realm="imag"');
        header('HTTP/1.0 401 Unauthorized');
        exit;
    } 
    else 
    {
        if ($_SERVER['PHP_AUTH_USER'] != $username || $_SERVER['PHP_AUTH_PW'] != $passwd)
        {
            header('WWW-Authenticate: Basic realm="imag"');       
        }
    }    
}   
?>

