<?php 
//需要授权才能访问本页面
http_auth();

@header("Content-type: text/html;charset=UTF-8");

$connection    = @mysql_connect('localhost', 'root', 'daningmysql2006');
$db            =  mysql_select_db("campaign", $connection);    
mysql_query(" set names utf8 ");

export();

function export()
{
    global $connection;
    require_once 'PHPExcel.php';  
    require_once 'PHPExcel/Writer/Excel2007.php';    // 用于其他低版本xls  
    $objExcel = new PHPExcel();  
    $objWriter = new PHPExcel_Writer_Excel5($objExcel);    // 用于其他版本格式  
    $objExcel->setActiveSheetIndex(0);    
    $objActSheet = $objExcel->getActiveSheet();  
    $objActSheet->setTitle('金桥'.date("Y-m-d"));

    $objExcel->getActiveSheet()->getColumnDimension('B')->setWidth('20');
    $objExcel->getActiveSheet()->getColumnDimension('C')->setWidth('20');
    $objExcel->getActiveSheet()->getColumnDimension('D')->setWidth('30');           
    $objExcel->getActiveSheet()->getColumnDimension('E')->setWidth('30');
    $objExcel->getActiveSheet()->getStyle('Q2:Q3000')->getAlignment()->setWrapText(true);  
    
    $objActSheet->setCellValue('A1', 'ID');  
    $objActSheet->setCellValue('B1', '大宁会员卡号');           
    $objActSheet->setCellValue('C1', '姓名');        
    $objActSheet->setCellValue('D1', '性别'); 
    $objActSheet->setCellValue('E1', '手机'); 
    $objActSheet->setCellValue('F1', '家庭（单位）地址'); 
    $objActSheet->setCellValue('G1', '所属区域');
    $objActSheet->setCellValue('H1', 'A1'); 
    $objActSheet->setCellValue('I1', 'A2'); 
    $objActSheet->setCellValue('J1', 'A3'); 
    $objActSheet->setCellValue('K1', 'A4'); 
    $objActSheet->setCellValue('L1', 'A5'); 
    $objActSheet->setCellValue('M1', 'B1'); 
    $objActSheet->setCellValue('N1', 'B2'); 
    $objActSheet->setCellValue('O1', 'B3'); 
    $objActSheet->setCellValue('P1', 'B4'); 
    $objActSheet->setCellValue('Q1', 'B5'); 
    $objActSheet->setCellValue('R1', 'B6');
    $objActSheet->setCellValue('S1', 'B7');
    $objActSheet->setCellValue('T1', 'C1');
    $objActSheet->setCellValue('U1', 'C2'); 
    $objActSheet->setCellValue('V1', 'C3');
    $objActSheet->setCellValue('W1', 'C4');
    $objActSheet->setCellValue('X1', 'C5');
    $objActSheet->setCellValue('Y1', 'C6'); 
    $objActSheet->setCellValue('Z1', 'C7');
    
    $objActSheet->setCellValue('AA1', 'C8');  
    $objActSheet->setCellValue('AB1', 'C9');           
    $objActSheet->setCellValue('AC1', 'C10');        
    $objActSheet->setCellValue('AD1', 'C11'); 
    $objActSheet->setCellValue('AE1', 'C12');
     
    $objActSheet->setCellValue('AF1', 'C13'); 
    $objActSheet->setCellValue('AG1', 'C14');
    
    $objActSheet->setCellValue('AH1', 'D1'); 
    $objActSheet->setCellValue('AI1', 'D2'); 
    $objActSheet->setCellValue('AJ1', 'D3'); 
    $objActSheet->setCellValue('AK1', 'D4'); 
    $objActSheet->setCellValue('AL1', 'D5'); 
    $objActSheet->setCellValue('AM1', 'D6'); 
    $objActSheet->setCellValue('AN1', 'D7');  
    
    $objActSheet->setCellValue('AO1', 'D8'); 
    $objActSheet->setCellValue('AP1', 'D9'); 
    $objActSheet->setCellValue('AQ1', 'E1'); 
    $objActSheet->setCellValue('AR1', 'E2');
    $objActSheet->setCellValue('AS1', 'E3');
    $objActSheet->setCellValue('AT1', '对大宁国际的建议');
    $objActSheet->setCellValue('AU1', '提交时间');       
                                      
         
    $sql = "SELECT * FROM  2012dcwj ORDER BY uid";
    $result = mysql_query($sql, $connection);
    $i = 2;
    while ($row = mysql_fetch_array($result))
    {
        $objActSheet->setCellValue('A'.$i, $row['uid']);  
        $objActSheet->setCellValue('B'.$i, $row['vipcode']);           
        $objActSheet->setCellValue('C'.$i, " ".$row['name']);          
        $objActSheet->setCellValue('D'.$i, " ".$row['sex']); 
        $objActSheet->setCellValue('E'.$i, $row['mobile']);
        
        $objActSheet->setCellValue('F'.$i, $row['address']);
        $objActSheet->setCellValue('G'.$i, $row['home_area']);
        $objActSheet->setCellValue('H'.$i, $row['a_age']);
        $objActSheet->setCellValue('I'.$i, $row['a_home_structure']);
        $objActSheet->setCellValue('J'.$i, $row['a_month_income']);
        $objActSheet->setCellValue('K'.$i, $row['a_job']);
        $objActSheet->setCellValue('L'.$i, $row['a_edu']);
        $objActSheet->setCellValue('N'.$i, $row['b1']);
        $objActSheet->setCellValue('M'.$i, $row['b2']);
        $objActSheet->setCellValue('O'.$i, $row['b3']);
        $objActSheet->setCellValue('P'.$i, $row['b4']);
        $objActSheet->setCellValue('Q'.$i, str_replace(',', ',', $row['b5']));
        $objActSheet->setCellValue('R'.$i, $row['b6']);
        $objActSheet->setCellValue('S'.$i, $row['b7']);
        $objActSheet->setCellValue('T'.$i, $row['c1']);
        $objActSheet->setCellValue('U'.$i, str_replace(';', ';', $row['c2']));
        $objActSheet->setCellValue('V'.$i, str_replace(',', ',', $row['c3']));
        $objActSheet->setCellValue('W'.$i, $row['c4']);
        $objActSheet->setCellValue('X'.$i, $row['c5']);
        $objActSheet->setCellValue('Y'.$i, $row['c6']);
        $objActSheet->setCellValue('Z'.$i, $row['c7']);
        $objActSheet->setCellValue('AA'.$i, $row['c8']);
        $objActSheet->setCellValue('AB'.$i, str_replace(',', ',', $row['c9']));
        $objActSheet->setCellValue('AC'.$i, str_replace(',', ',', $row['c10']));
        $objActSheet->setCellValue('AD'.$i, $row['c11']);
        $objActSheet->setCellValue('AE'.$i, str_replace(',', ',', $row['c12']));
        $objActSheet->setCellValue('AF'.$i, str_replace(',', ',', $row['c13']));
        $objActSheet->setCellValue('AG'.$i, $row['c14']);
        $objActSheet->setCellValue('AH'.$i, str_replace(';', ',', $row['d1']));
        $objActSheet->setCellValue('AI'.$i, $row['d2']);
        $objActSheet->setCellValue('AJ'.$i, str_replace(';', ',', $row['d3']));
        $objActSheet->setCellValue('AK'.$i, $row['d4']);
        $objActSheet->setCellValue('AL'.$i, $row['d5']);
        $objActSheet->setCellValue('AN'.$i, $row['d6']);
        $objActSheet->setCellValue('AM'.$i, str_replace(';', ';', $row['d7']));
        $objActSheet->setCellValue('AO'.$i, str_replace(';', ';', $row['d8']));
        $objActSheet->setCellValue('AP'.$i, $row['d9']);
        $objActSheet->setCellValue('AQ'.$i, str_replace(',', ',', $row['e1']));
        $objActSheet->setCellValue('AR'.$i, str_replace(',', ',', $row['e2']));
        $objActSheet->setCellValue('AS'.$i, str_replace(',', ',', $row['e3']));
        $objActSheet->setCellValue('AT'.$i, $row['e4']);
        $objActSheet->setCellValue('AU'.$i, $row['time']);                                                                       
        $i++;         
    }
      
    $outputFileName = "data/daning_".date("Y-m-d").".xls";  
    $objWriter->save($outputFileName); 
    echo("<a href='".$outputFileName."' target='_blank'>点击下载大宁调研问卷数据</a>");
}

function http_auth()
{
    //需要http认证才能访问本页面
    $username = 'kimi';
    $passwd   = '1111';

    if (!isset($_SERVER['PHP_AUTH_USER'])) 
    {
        header('WWW-Authenticate: Basic realm="imag"');
        header('HTTP/1.0 401 Unauthorized');
        exit;
    } 
    else 
    {
        if ($_SERVER['PHP_AUTH_USER'] != $username || $_SERVER['PHP_AUTH_PW'] != $passwd)
        {
            header('WWW-Authenticate: Basic realm="imag"');       
        }
    }    
}   
?>

